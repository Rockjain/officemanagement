
var environments ={}

environments.staging={
        port: "http://localhost:3002",
        envName:'staging',  
}

environments.production = {
        port: "http://165.22.208.241:3002",
        envName:'production',   
}

// Determine which environment was passed as a command-line argument
var currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

// Check that the current environment is one of the environments above, if not default to staging
var config = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

console.log(config)

export default config;