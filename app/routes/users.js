var express = require('express');
var router = express.Router();
var md5 = require('md5');
var path = require('path');
var Request = require("request");
const sequelize = require("../models/User")
const jwt = require("jsonwebtoken")
var PHPUnserialize = require('php-unserialize');
var phpUnserialize = require('phpunserialize');
var serialize = require('node-serialize');
const Serialize = require('php-serialize');
var lowerCase = require('lower-case');
const multer = require('multer');
var formidable = require('formidable');
var fs = require('fs');
/* GET users listing. */

process.env.SECRET_KEY = 'secret'

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
}); 

router.get('/admin',function(req,res){
  sequelize.query("SELECT id, username, ragistered, role, status FROM admin",{type: sequelize.QueryTypes.SELECT, raw: true}).then(rows=>{
    res.json(rows);
    console.log(rows);
  }) 
})

router.post('/login',function(req,res){
  let username = req.body.username;
  sequelize.query("SELECT * FROM admin WHERE username=? and password =?",{type:sequelize.QueryTypes.SELECT, raw:true,  replacements: [
    username, md5(req.body.password)
  ]}).then(rows => {
    // console.log(rows)
     // authId = rows[0].auth_id;
    
      if (rows.length == 0) {
        res.json({
          success:false,
          message:'UserId not exist'
        });
      } else {
        let id = rows[0].id;
        let token = jwt.sign({username: username, id:id}, process.env.SECRET_KEY, {
          expiresIn: '24h'
        })
        res.json({
          success: true,
          message: 'Authentication successful!',
          token: token,
          rows
        });
      }
    })
})

const checkToken = (req, res, next) => {
  const header = req.headers['authorization'];

  if (header !== 'undefined') {
    const token = header;
    req.token = token;
    next();
  } else {
    res.sendStatus(403)
  }
}

router.get('/profile',checkToken,function(req,res){
  let token = jwt.verify(req.token, process.env.SECRET_KEY)

  sequelize.query("SELECT * FROM admin WHERE id = ?",{type:sequelize.QueryTypes.SELECT, raw:true,  replacements:[token.id]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.post('/editprofile',function(req,res){
  sequelize.query("UPDATE admin SET password =? WHERE id=? and password = ?",{type:sequelize.QueryTypes,raw:true,replacements:[md5(req.body.newpassword),req.body.id,md5(req.body.password)]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.post('/addemployee',function(req,res){
  let updated_date = Date.now();
  let accountinfo ={
    account_name:req.body.account_name,
    account_number:req.body.account_number,
    banck_name:req.body.bank_name,
    branch_name:req.body.branch_name,
    ifsccode:req.body.ifsccode
  }

  let account_info =  Serialize.serialize(accountinfo)
  let preivious_experience = Serialize.serialize(req.body.preivious_experience);
  console.log("work",account_info,preivious_experience);
 // console.log("work",PHPUnserialize.unserialize(account_info)); 
 // console.log(req.body.parmanent_address_l1,req.body.parmanent_address_l2,req.body.parmanent_city,req.body.parmanent_state,req.body.parmanent_country,req.body.parmanent_pincode,req.body.local_address_l1,req.body.local_address_l2,req.body.local_city,req.body.local_state,req.body.local_country,req.body.local_pincode) 
  
  //console.log("work",PHPUnserialize.unserialize(datas));

  sequelize.query("INSERT INTO employe (name,father_name,dob,gender,parmanent_address_l1,parmanent_address_l2,parmanent_city,parmanent_state,parmanent_country,parmanent_pincode,local_address_l1,local_address_l2,local_city,local_state,local_country,local_pincode,pan_card_no,adhar_card_no,emp_code,joining_date,preivious_experience,updated_date,department,account_info,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
  {type:sequelize.QueryTypes.INSERT,raw:true,replacements:[
    req.body.name,req.body.father_name,req.body.dob,req.body.gender,req.body.parmanent_address_l1,req.body.parmanent_address_l2,req.body.parmanent_city,req.body.parmanent_state,req.body.parmanent_country,req.body.parmanent_pincode,req.body.local_address_l1,req.body.local_address_l2,req.body.local_city,req.body.local_state,req.body.local_country,req.body.local_pincode,req.body.pan_card,req.body.adhar_card,req.body.emp_code,req.body.joining_date,preivious_experience,updated_date,req.body.department,
    account_info,req.body.status
  ]}).then(rows=>{
    console.log(rows)
    res.json({rows,success:true})

  })
})

router.post('/updateemployee',function(req,res){
 
  let updated_date = Date.now();
  let accountinfo ={
    account_name:req.body.account_name,
    account_number:req.body.account_number,
    banck_name:req.body.bank_name,
    branch_name:req.body.branch_name,
    ifsccode:req.body.ifsccode
  }

  let account_info =  Serialize.serialize(accountinfo)
  let preivious_experience = Serialize.serialize(req.body.preivious_experience);

  sequelize.query("UPDATE employe SET name=?,father_name=?,dob=?,gender=?,parmanent_address_l1=?,parmanent_address_l2=?,parmanent_city=?,parmanent_state=?,parmanent_country=?,parmanent_pincode=?,local_address_l1=?,local_address_l2=?,local_city=?,local_state=?,local_country=?,local_pincode=?,pan_card_no=?,adhar_card_no=?,emp_code=?,joining_date=?,releiving_date=?,preivious_experience=?,updated_date=?,department=?,account_info=?,status=? where id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[
    req.body.name,req.body.father_name,req.body.dob,req.body.gender,req.body.parmanent_address_l1,req.body.parmanent_address_l2,req.body.parmanent_city,req.body.parmanent_state,req.body.parmanent_country,req.body.parmanent_pincode,req.body.local_address_l1,req.body.local_address_l2,req.body.local_city,req.body.local_state,req.body.local_country,req.body.local_pincode,req.body.pan_card,req.body.adhar_card,req.body.emp_code,req.body.joining_date,req.body.releiving_date,preivious_experience,updated_date,req.body.department,account_info,req.body.status,req.body.id
  ]}).then(rows=>{
    res.json({rows,success:true})
  }) 
})

router.get('/showemployee',function(req,res){
  sequelize.query("SELECT * FROM employe WHERE status != ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:["delete"]}).then(rows=>{
   // console.log(rows)
    res.json(rows)
   
  })
})

router.get('/currentemp',function(req,res){
  sequelize.query("SELECT * FROM employe WHERE status = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:["current employee"]}).then(rows=>{
    // console.log(rows)
     res.json(rows)
    
   })
})

router.get('/getEmployeeById',function(req,res){
  let id= req.param('id')

  sequelize.query("SELECT * FROM employe WHERE id = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id]}).then(rows=>{
    console.log(rows)
    let bankdetails = Object.values(PHPUnserialize.unserialize(rows[0].account_info))
    let preivious_experience = Object.values(PHPUnserialize.unserialize(rows[0].preivious_experience))
    res.json({data:rows,account_info:bankdetails,preivious_experience:preivious_experience})
   
  })
})

router.post('/deleteEmployee',function(req,res){
  sequelize.query("UPDATE employe set status=? WHERE id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:["delete",req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.get('/department',function(req,res){
  sequelize.query("SELECT * From department",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    res.json(rows)
  })
})

router.get('/subdepartment',function(req,res){
  sequelize.query("SELECT child.*, parent.name AS parent_name FROM `department` AS child left join department AS parent on child.parent_id = parent.id",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    res.json(rows)
  })
})

router.post('/adddepartment',function(req,res){
  let date = Date.now();
  sequelize.query("INSERT INTO department (name,updated_date,parent_id) VALUES (?,?,?)",{type:sequelize.QueryTypes.INSERT,raw:true,replacements:[req.body.department,date,req.body.parent_id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.post('/editdepartment',function(req,res){
  let date=Date.now();
  sequelize.query("UPDATE department set name=?,parent_id=?,updated_date=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[req.body.name,req.body.parent_id,date,req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.delete('/deletedepartment',function(req,res){
  let id = req.param("id")

  sequelize.query("DELETE FROM department WHERE id=?",{type:sequelize.QueryTypes.DELETE,raw:true,replacements:[id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  
  })
})

router.get('/deletemultipledepartment',function(req,res){
  let id = req.param("ids");
  let ids = JSON.parse(id);
  let d_id = ids.map((data)=>data.id)
  console.log("seat_id",id,ids,d_id)
  
    sequelize.query("SELECT parent_id FROM department WHERE parent_id IN ("+d_id+")",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
      console.log(rows)
      let sub_id=[]
      let row;
      if(rows.length>0){
      for(let i=0;i<rows.length;i++){
        let id = rows[i].parent_id
        sequelize.query("SELECT parent_id FROM department WHERE id = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id]}).then(rows=>{
          console.log(rows)
          sequelize.query("UPDATE department set parent_id = ? WHERE parent_id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[rows[0].parent_id,id]}).then(rows=>{
            row = "true";
          })
        })
      }
      sequelize.query("DELETE FROM department WHERE id IN ("+d_id+")",{type:sequelize.QueryTypes.DELETE,raw:true}).then(rows=>{
        console.log(rows)
        res.json({success:true})
      })
    }else{
      sequelize.query("DELETE FROM department WHERE id IN ("+d_id+")",{type:sequelize.QueryTypes.DELETE,raw:true}).then(rows=>{
         console.log(rows)
         res.json({success:true})
       
       })
    }
      
    })

})


router.post('/deletemultipleEmployee',function(req,res){
  let id = req.body.ids;
  let ids = JSON.parse(id);
  let e_id = ids.map((data)=>data.id)
  console.log("seat_id",id,ids,e_id)

  sequelize.query("UPDATE employe set status=? WHERE id IN ("+e_id+")",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:["delete"]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })

})

router.get('/searchdep',function(req,res){
    let val = req.param("val");
    let value = lowerCase(val)
    console.log(value)
      sequelize.query("SELECT child.*, parent.name AS parent_name FROM `department` AS child left join department AS parent on child.parent_id = parent.id WHERE LOWER(child.name) LIKE '%"+value+"%' OR LOWER(parent.name) LIKE '%"+value+"%'",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
        console.log(rows)
        res.json(rows)
      })
    
})

router.get('/searchemp',function(req,res){
  let val = req.param("val");
  let value = lowerCase(val)
  console.log(value)
    sequelize.query("SELECT * FROM employe WHERE LOWER(name) LIKE '%"+value+"%' OR LOWER(status) LIKE '%"+value+"%' OR LOWER(department) LIKE '%"+value+"%'",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
      console.log(rows)
      res.json(rows)
    })
  
})

router.get('/salary',function(req,res){
  sequelize.query("SELECT salary.*,employe.name AS emp_name,employe.gender, employe.status AS emp_status, employe.id AS emp_id FROM `salary` LEFT JOIN `employe` ON employe.id=emp_id ",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    res.json(rows);
  })
})

router.get('/searchsalary',function(req,res){
   let val = req.param("val");
  let value = lowerCase(val)

  sequelize.query("SELECT salary.*,employe.name AS emp_name,employe.gender, employe.status AS emp_status, employe.id AS emp_id FROM `salary` LEFT JOIN `employe` ON employe.id=emp_id WHERE LOWER(employe.name) LIKE '%"+value+"%'",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    res.json(rows);
  })
})

router.post('/addsalary',function(req,res){
  sequelize.query("SELECT emp_code from employe WHERE id= ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[req.body.emp_id]}).then(rows=>{
    console.log(rows)
    if(rows.length>0){
      if(req.body.status === "active"){
      sequelize.query("UPDATE `salary` set status=? WHERE emp_id=?",{type:sequelize.QueryTypes.UPDATE,replacements:["deactive",req.body.emp_id]}).then(row=>{
        console.log(row)
        sequelize.query("INSERT INTO salary (emp_id,emp_code,apply_date,status,basic_salary,hra,transportation_allowance,medical_allowance,empr_pf_contri,emp_pf_contri,esi_of_empr_contri,esi_of_emp_contri) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",{type:sequelize.QueryTypes.INSERT,raw:true,replacements:[
          req.body.emp_id, rows[0].emp_code, req.body.apply_date, req.body.status, req.body.basic_salary, req.body.hra, req.body.transportation_allowance, req.body.medical_allowance, req.body.empr_pf_contri, req.body.emp_pf_contri, req.body.esi_of_empr_contri, req.body.esi_of_emp_contri 
        ]}).then(rows=>{
          console.log(req.body.emp_id)
          res.json({success:true})
        })
      })
    }else{
    sequelize.query("INSERT INTO salary (emp_id,emp_code,apply_date,status,basic_salary,hra,transportation_allowance,medical_allowance,empr_pf_contri,emp_pf_contri,esi_of_empr_contri,esi_of_emp_contri) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",{type:sequelize.QueryTypes.INSERT,raw:true,replacements:[
      req.body.emp_id, rows[0].emp_code, req.body.apply_date, req.body.status, req.body.basic_salary, req.body.hra, req.body.transportation_allowance, req.body.medical_allowance, req.body.empr_pf_contri, req.body.emp_pf_contri, req.body.esi_of_empr_contri, req.body.esi_of_emp_contri 
    ]}).then(rows=>{
      console.log(req.body.emp_id)
      res.json({success:true})
    })
  }

    }
  })
 
})

router.get('/salarybyid',function(req,res){
  let id = req.param("id")
  sequelize.query("SELECT salary.*,employe.name AS emp_name,employe.gender, employe.status AS emp_status, employe.id AS emp_id FROM `salary` LEFT JOIN `employe` ON employe.id=emp_id WHERE salary.id = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id]}).then(rows=>{
    res.json(rows);
  })
})

router.post('/updatesalary',function(req,res){

  sequelize.query("UPDATE `salary` set apply_date=?,status=?,basic_salary=?,hra=?,transportation_allowance=?,medical_allowance=?,empr_pf_contri=?,emp_pf_contri=?,esi_of_empr_contri=?,esi_of_emp_contri=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[req.body.apply_date, req.body.status, req.body.basic_salary, req.body.hra, req.body.transportation_allowance,req.body.medical_allowance, req.body.empr_pf_contri, req.body.emp_pf_contri, req.body.esi_of_empr_contri, req.body.esi_of_emp_contri, req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })

})

router.delete('/deletesalary',function(req,res){
  let id=req.param("id")
  sequelize.query("DELETE FROM salary WHERE id=?",{type:sequelize.QueryTypes.DELETE,raw:true,replacements:[id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.post('/updateSalaryStatus',function(req,res){
  sequelize.query("UPDATE `salary` set status=? WHERE emp_id=?",{type:sequelize.QueryTypes.UPDATE,replacements:["deactive",req.body.emp_id]}).then(rows=>{
    console.log(rows)
    res.json({rows,success:true})
  })
})

router.post('/toggleStatus',function(req,res){ 
  sequelize.query("SELECT status FROM `salary` WHERE id=?",{type:sequelize.QueryTypes.SELECT,replacements:[req.body.id]}).then(rows=>{
    console.log(rows[0].status)
    if(rows[0].status == "active"){
      sequelize.query("UPDATE `salary` set status=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,replacements:["deactive",req.body.id]}).then(rows=>{
        res.json({success:true})
      })
    }else{
      sequelize.query("UPDATE `salary` set status=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,replacements:["active",req.body.id]}).then(rows=>{
        sequelize.query("UPDATE `salary` set status=? WHERE id != ? and emp_id=?",{type:sequelize.QueryTypes.UPDATE,replacements:["deactive",req.body.id,req.body.emp_id]}).then(rows=>{
          res.json({success:true})
        })
      })
    }
  }) 
})

router.get('/activesalary',(req,res)=>{
  sequelize.query("SELECT salary.*,employe.name AS emp_name,employe.gender, employe.status AS emp_status, employe.id AS emp_id FROM `salary` LEFT JOIN `employe` ON employe.id=emp_id WHERE salary.status = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:["active"]}).then(rows=>{
    res.json(rows);
  }) 
})

router.get('/salaryTransaction',function(req,res){
  let month = req.param("month")
  let year = req.param("year")
  console.log(month,year)
  sequelize.query("SELECT salary_transaction.*, employe.id AS emp_id, employe.name AS emp_name, employe.status AS emp_status, employe.gender AS gender from salary_transaction LEFT JOIN employe ON employe.id = emp_id WHERE MONTH(paid_date) = ? AND YEAR(paid_date) = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[month,year]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get('/searchTransaction',function(req,res){
  let month = req.param("month")
  let year = req.param("year")
  let val = req.param("val");
  let value = lowerCase(val)
  console.log(month,year)
  sequelize.query("SELECT salary_transaction.*, employe.id AS emp_id, employe.name AS emp_name, employe.status AS emp_status, employe.gender AS gender from salary_transaction LEFT JOIN employe ON employe.id = emp_id WHERE MONTH(paid_date) = ? AND YEAR(paid_date) = ? AND LOWER(employe.name) LIKE  '%"+value+"%'",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[month,year]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.post('/addtransaction',(req,res)=>{
  console.log("emp_id",req.body.emp_id)
  let date = new Date();
  let month = date.getMonth()+1;
  console.log(month)
  sequelize.query("SELECT * FROM salary_transaction WHERE emp_id = ? and MONTH(paid_date)=?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[req.body.emp_id,month]}).then(rows=>{
    console.log(rows.length)
    if(rows.length>0){
      res.json({success:false})
    }else{
        sequelize.query("INSERT INTO salary_transaction (emp_id,emp_code,paid_date,paid_status,total_paid,basic_salary,hra,transportation_allowance,medical_allowance,bonus,empr_pf_contri,emp_pf_contri,esi_of_empr_contri,esi_of_emp_contri,advance,tds,absent_short_time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",{type:sequelize.QueryTypes.INSERT,raw:true,replacements:[
    req.body.emp_id, req.body.emp_code, req.body.paid_date, req.body.paid_status, req.body.total_paid, req.body.basic_salary, req.body.hra, req.body.transportation_allowance, req.body.medical_allowance, req.body.bonus, req.body.empr_pf_contri, req.body.emp_pf_contri, req.body.esi_of_empr_contri, req.body.esi_of_emp_contri, req.body.advance, req.body.tds, req.body.absent_short_time
  ]}).then(rows=>{
    console.log(rows)
      res.json({success:true})
    })
    }
  })

})

router.post("/updatetransaction",(req,res)=>{
  let date = new Date();
  let month = date.getMonth()+1;
  sequelize.query("SELECT id FROM salary_transaction WHERE emp_id = ? and MONTH(paid_date)=?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[req.body.emp_id,month]}).then(rows=>{
    console.log(rows[0].id)
    let id = rows[0].id
    sequelize.query("UPDATE salary_transaction set paid_date=?, paid_status=?, total_paid=?, bonus=?, advance=?, tds=?, absent_short_time=? WHERE id = ? AND paid_status = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[req.body.paid_date, req.body.paid_status, req.body.total_paid, req.body.bonus, req.body.advance, req.body.tds, req.body.absent_short_time, id, "unpaid"]}).then(rows=>{
      console.log(rows)
      res.json({success:true})
    })
  })
 
})

router.post("/togglepaid",(req,res)=>{
 
      sequelize.query("UPDATE `salary_transaction` set paid_status=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,replacements:["paid",req.body.id]}).then(rows=>{
          res.json({success:true})
      })

})

router.get("/birthday",(req,res)=>{
  sequelize.query("SELECT MONTH(dob) As month, DAY(dob) AS day, name from employe",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    res.json(rows)
  })
})

router.get("/salaryslip",(req,res)=>{
  let id = req.param("id");

  sequelize.query("SELECT st.*, employe.*, childdpt.name AS designation ,parent.name AS department from salary_transaction AS st JOIN employe on employe.id = st.emp_id JOIN department AS childdpt on childdpt.id = employe.department LEFT JOIN department AS parent ON childdpt.parent_id = parent.id WHERE st.id=?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get("/no_of_salaryslip",(req,res)=>{
  let id = req.param("id");
  let ids = JSON.parse(id);
  let s_id = ids.map((data)=>data.id)
  console.log("id",id,ids,s_id)

  sequelize.query("SELECT st.*, employe.*, childdpt.name AS designation ,parent.name AS department from salary_transaction AS st JOIN employe on employe.id = st.emp_id JOIN department AS childdpt on childdpt.id = employe.department LEFT JOIN department AS parent ON childdpt.parent_id = parent.id WHERE st.id IN ("+s_id+")",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get("/all_salaryslip_by_month",(req,res)=>{

  let from = req.param("from")
  let to = req.param("to")
  let yearfrom = req.param("yearfrom")
  let yearto = req.param("yearto")
  

  sequelize.query("SELECT st.*, employe.*, childdpt.name AS designation ,parent.name AS department from salary_transaction AS st JOIN employe on employe.id = st.emp_id JOIN department AS childdpt on childdpt.id = employe.department LEFT JOIN department AS parent ON childdpt.parent_id = parent.id WHERE MONTH(st.paid_date) BETWEEN ? AND ? AND YEAR(st.paid_date) BETWEEN ? AND ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[from, to, yearfrom , yearto]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get("/salaryslip_by_month",(req,res)=>{
  let id = req.param("id");

  let from = req.param("from")
  let to = req.param("to")
  let yearfrom = req.param("yearfrom")
  let yearto = req.param("yearto")

  sequelize.query("SELECT st.*, employe.*, childdpt.name AS designation ,parent.name AS department from salary_transaction AS st JOIN employe on employe.id = st.emp_id JOIN department AS childdpt on childdpt.id = employe.department LEFT JOIN department AS parent ON childdpt.parent_id = parent.id WHERE st.emp_id = ? AND MONTH(st.paid_date) BETWEEN ? AND ?  AND YEAR(st.paid_date) BETWEEN ? AND ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id, from, to, yearfrom , yearto]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get("/finalreport",(req,res)=>{
  let from = req.param("fromdate");
  let to = req.param("todate");

  sequelize.query("SELECT st.*, MONTH(st.paid_date) AS paid_month, employe.name AS name FROM `salary_transaction` AS st JOIN employe ON employe.id = st.emp_id WHERE st.paid_date BETWEEN ? AND ?",{type:sequelize.QueryTypes.SELECT,raw:true, replacements:[from,to]}).then(rows=>{
    res.json(rows)
  })
})

router.get("/showtask",(req,res)=>{
  
  sequelize.query("SELECT task.*, employe.name AS name, employe.gender AS gender FROM task join employe ON employe.id = emp_id ORDER BY date DESC",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get("/showtaskbyid",(req,res)=>{
    let id = req.param("id")
    sequelize.query("SELECT task.*, employe.name AS name, employe.gender AS gender FROM task join employe ON employe.id = emp_id WHERE task.id = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id]}).then(rows=>{
      console.log(rows)
      res.json(rows)
    })
})

router.get("/searchtask",(req,res)=>{
  let val = req.param("val");
  let value = lowerCase(val)
  
  sequelize.query("SELECT task.*, employe.name AS name, employe.gender AS gender FROM task join employe ON employe.id = emp_id WHERE LOWER(employe.name) LIKE '%"+value+"%' ORDER BY date DESC",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.get("/searchtaskbydate",(req,res)=>{
  let get_date = req.param("date")
  let date = new Date(get_date)
 let day = date.getDate();
 let month = date.getMonth()+1;
 let year = date.getFullYear();
 console.log(day,month,year)
  
  sequelize.query("SELECT task.*, employe.name AS name, employe.gender AS gender FROM task join employe ON employe.id = emp_id WHERE DAY(date)=? AND MONTH(date)=? AND YEAR(date)=?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[day,month,year]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.post("/addtask",(req,res)=>{
 let date = new Date(req.body.date)
 let day = date.getDate();
  console.log(day)
    sequelize.query("SELECT * FROM task WHERE emp_id = ? and DAY(date)=?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[req.body.emp_id,day]}).then(rows=>{
      console.log(rows.length)
      if(rows.length>0){
        res.json({success:false})
      }else{
      sequelize.query("INSERT INTO task (emp_id, emp_code, date, time, project, task, status) VALUES (?,?,?,?,?,?,?)",{type:sequelize.QueryTypes.INSERT, raw:true, replacements:[
        req.body.emp_id,req.body.emp_code, req.body.date, req.body.time, req.body.project, req.body.task, req.body.status
     ]}).then(rows=>{
        console.log(rows)
        res.json({success:true})
      })
    } 
  })
  })

router.post("/updateTasksSatus",(req,res)=>{

  sequelize.query("UPDATE `task` set status = ? WHERE id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:["completed",req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.post("/updateTask",(req,res)=>{
  console.log(req.body.task,req.body.id)

  sequelize.query("UPDATE `task` set task = ? WHERE id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[req.body.task,req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.delete("/deletetask",(req,res)=>{
let id = req.param("id")
  sequelize.query("DELETE FROM `task` WHERE id = ?",{type:sequelize.QueryTypes.DELETE,raw:true,replacements:[id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.post("/updateTask",(req,res)=>{
  console.log(req.body.task,req.body.id)

  sequelize.query("UPDATE `task` set task = ? WHERE id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[req.body.task,req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.post("/updateTaskTable",(req,res)=>{
  console.log(req.body.task,req.body.id)

  sequelize.query("UPDATE `task` set project=?, task = ?, time=?, status=? WHERE id = ?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[req.body.project, req.body.task, req.body.time, req.body.status, req.body.id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.delete("/deletetask",(req,res)=>{
let id = req.param("id")
  sequelize.query("DELETE FROM `task` WHERE id = ?",{type:sequelize.QueryTypes.DELETE,raw:true,replacements:[id]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})


router.delete('/deletemultipletask',function(req,res){
  let id = req.param("ids");
  let ids = JSON.parse(id);
  let t_id = ids.map((data)=>data.id)
  console.log("task_id",id,ids,t_id)

  sequelize.query("DELETE FROM `task` WHERE id IN ("+t_id+")",{type:sequelize.QueryTypes.DELETE,raw:true}).then(rows=>{
    console.log(rows)
     res.json({success:true})
  })

})

router.delete('/deletemultiplesalary',function(req,res){
  let id = req.param("ids");
  let ids = JSON.parse(id);
  let t_id = ids.map((data)=>data.id)
  console.log("task_id",id,ids,t_id)

  sequelize.query("DELETE FROM `salary` WHERE id IN ("+t_id+")",{type:sequelize.QueryTypes.DELETE,raw:true}).then(rows=>{
    console.log(rows)
     res.json({success:true})
  })

})

router.post('/addclient',(req,res)=>{
  console.log(req.body)
  let detail = {
    other_detail_1:req.body.other_detail_1,
    other_detail_2:req.body.other_detail_2,
    other_detail_3:req.body.other_detail_3,
    other_detail_4:req.body.other_detail_4,
  }

  let other_contact_detail =  Serialize.serialize(detail)
  
  console.log("contact",other_contact_detail);

  sequelize.query("INSERT INTO client (name, address_line_1, address_line_2, city, state, country, pin_code, contact_no_1, contact_no_2, email_1, email_2, skype_id, other_contact_detail) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",{type:sequelize.QueryTypes.INSERT,raw:true,replacements:[req.body.name, req.body.address_line_1, req.body.address_line_2, req.body.city, req.body.state, req.body.country, req.body.pincode, req.body.contact_no_1, req.body.contact_no_2, req.body.email_1, req.body.email_2, req.body.skype, other_contact_detail ]}).then(rows=>{
    console.log(rows)
    res.json({success:true})
  })
})

router.get('/showclient',(req,res)=>{
  sequelize.query("SELECT * FROM client",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
   // console.log(rows)
    res.json(rows)
  })
})

router.delete('/deleteclient',(req,res)=>{
  let id = req.param("id")
  sequelize.query("DELETE FROM client WHERE id = ?",{type:sequelize.QueryTypes.DELETE,raw:true,replacements:[id]}).then(rows=>{
    res.json({success:true})
  })
})

router.get('/clientbyid',(req,res)=>{
  let id = req.param("id")

  sequelize.query("SELECT * FROM client WHERE id = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:[id]}).then(rows=>{
    // console.log(rows)
    let contact_details ;
    if(rows[0].other_contact_detail !== null){
      contact_details = Object.values(PHPUnserialize.unserialize(rows[0].other_contact_detail))
    }
     res.json({rows:rows,contact_details:contact_details})
   })
})

router.post('/updateclient',(req,res)=>{
  let detail = {
    other_detail_1:req.body.other_detail_1,
    other_detail_2:req.body.other_detail_2,
    other_detail_3:req.body.other_detail_3,
    other_detail_4:req.body.other_detail_4,
  }

  let other_contact_detail =  Serialize.serialize(detail)

  sequelize.query("UPDATE client set name=?, address_line_1=?, address_line_2=?, city=?, state=?, country=?, pin_code=?, contact_no_1=?, contact_no_2=?, email_1=?, email_2=?, skype_id=?, other_contact_detail=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,raw:true,replacements:[
    req.body.name, req.body.address_line_1, req.body.address_line_2, req.body.city, req.body.state, req.body.country, req.body.pincode, req.body.contact_no_1, req.body.contact_no_2, req.body.email_1, req.body.email_2, req.body.skype, other_contact_detail,req.body.id
  ]}).then(rows=>{
    res.json({success:true})
  })
})

router.get('/searchclient',(req,res)=>{

  let val = req.param("val");
  let value = lowerCase(val)
  
  sequelize.query("SELECT * FROM client WHERE LOWER(name) LIKE '%"+value+"%' ",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })

})

router.delete('/deletemultipleclient',(req,res)=>{
  let id = req.param("ids")
  let ids = JSON.parse(id);
  let c_id = ids.map((data)=>data.id)
  console.log(ids,c_id)

  sequelize.query("DELETE FROM `client` WHERE id IN ("+c_id+")",{type:sequelize.QueryTypes.DELETE,raw:true}).then(rows=>{
    console.log(rows)
     res.json({success:true})
  })
})

router.get('/projectTech',(req,res)=>{
  sequelize.query("SELECT * FROM project_tech",{type:sequelize.QueryTypes.SELECT,raw:true}).then(rows=>{
    res.json(rows)
  })
})


//uploadimage

const storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null,'public/images')
  },
  filename: function(req, file, cb){
     cb(null,file.originalname);
  }
});

const upload = multer({
  storage: storage,
}).array("myImage");

   router.post("/upload",(req,res)=>{
     upload(req,res,(err)=>{
    console.log("Request ---", req.body.name);
      console.log("Request file ---", req.file);//Here you get file.
      if(err) {
        return res.end("Error uploading file."+err);
    }
    res.json({success:true})
     })
   })


  router.post("/insertProject",(req,res)=>{
    console.log(req.body.file)
    let document = {
      file:req.body.file,
      images:req.body.images,
      discription:req.body.discription
    }
     let project_doc =  Serialize.serialize(document)
    console.log(document)

    sequelize.query("INSERT INTO project_management (name, client_id, project_tech, cost, project_from, project_doc, deadline, project_type_id,start_date, status) VALUES(?,?,?,?,?,?,?,?,?,?)",{type:sequelize.QueryTypes.INSERT,raw:true,replacements:[
      req.body.name, req.body.client_id, req.body.project_tech, req.body.cost, req.body.project_from, project_doc, req.body.deadline, req.body.project_type_id, req.body.start_date, req.body.status
    ]}).then(rows=>{
      console.log(rows)
      res.json(
       {success:true}
      ) 
    })
  })

  router.get("/project",(req,res)=>{

  sequelize.query("SELECT project_management.*, client.name AS client_name FROM project_management JOIN client ON project_management.client_id = client.id",{type:sequelize.QueryTypes.SELECT}).then(rows=>{
  
   // console.log(rows)
   
    res.json(rows)
  })
  })


  router.get("/projectbyid",(req,res)=>{
   let id = req.param("id")
    sequelize.query("SELECT project_management.*, client.name AS client_name FROM project_management JOIN client ON project_management.client_id = client.id WHERE project_management.id = ?",{type:sequelize.QueryTypes.SELECT,replacements:[id]}).then(rows=>{
      let document ;
      console.log(rows)
      if(rows[0].project_doc !== null){
        document = PHPUnserialize.unserialize(rows[0].project_doc);
      }
      console.log(document)
      res.json({rows,document})
      })
    })


  router.post("/updateproject",(req,res)=>{
    console.log(req.body)
    let document = {
      file:req.body.file,
      images:req.body.images,
      discription:req.body.discription
    }
     let project_doc =  Serialize.serialize(document)

    sequelize.query("UPDATE project_management set name=?, client_id=?, project_tech=?, cost=?, project_from=?, project_doc=?, deadline=?, project_type_id=?, start_date=?, end_date=?, status=? WHERE id=?",{type:sequelize.QueryTypes.UPDATE,raw:true, replacements:[
      req.body.name, req.body.client_id, req.body.project_tech, req.body.cost, req.body.project_from, project_doc, req.body.deadline, req.body.project_type_id, req.body.start_date,req.body.end_date, req.body.status, req.body.id
    ]}).then(rows=>{
      console.log(rows)
      res.json({success:true})
    })
  })

module.exports = router;
