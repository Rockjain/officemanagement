const Sequelize = require("sequelize")
const config = require('../routes/config')
const {db:{host, dialect, pool}} = config
const sequelize = new Sequelize("test", "root", "",{
    host:{host},
    dialect:{dialect},
    
    pool:{ 
       pool
    }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db