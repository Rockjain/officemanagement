export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',

    },
    {
      name: 'Employees',
      url: '/employee',
      icon: 'icon-people',
    },
    {
      name: 'Department',
      url: '/department',
      icon:'icon-plus'
    },
    {
      name: 'Salary Setup',
      url: '/salary',
      icon:'fa fa-inr'
    },
     {
      name: 'Salary',
      url: '/transaction',
      icon:'fa fa-inr'
    },
    
    {
      name: 'Report',
      url: '/report',
      icon:'fa fa-file-text',
      children: [
        {
          name: 'Salary Slip',
          url: '/report/salaryreport',
          icon: 'fa fa-file-text',
        },
        {
          name: 'Financial',
          url: '/report/finalreport',
          icon: 'fa fa-file-text',
        }]
    },
   {
    name:'Task',
    url:'/task',
    icon:'fa fa-tasks',
    children: [
      {
        name: 'Task Management',
        url: '/taskmanage',
        icon: 'fa fa-tasks',
      },
      {
        name: 'Task Threading',
        url: '/task',
        icon: 'fa fa-tasks',
      }]
   },
   {
    name:'Client Management',
    url:'/client',
    icon:'fa fa-handshake-o'
  },
  {
    name:'Project Management',
    url:'/project',
    icon:'fa fa-file-powerpoint-o'
  },
    {
      divider: true,
    },
 
  ],
};
