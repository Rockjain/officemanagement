import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import Moment from 'react-moment';
import config from '../../port.js';

class Profile extends Component{
    constructor(props){
        super(props)
        this.state = ({id:"",profiledata:[], modal: false,oldpassword:"",newpassword:"",confirmpassword:""})
        this.changepassword = this.changepassword.bind(this);
    }

    componentDidMount(){ 
        let token = localStorage.getItem("token");
       let header = {'Authorization':token};
       axios.get(config.port+"/users/profile",{
           headers:header
       }).then(res=>{
           console.log("profile",res.data)
           let id = res.data.map(data=>data.id);
           this.setState({id:id})
           this.setState({profiledata:res.data})
           console.log(this.state.id)
       })
    }

    changepassword() {
        this.setState({
          modal: !this.state.modal,
        });
      }

      myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      }

      editprofile=(event)=>{
        event.preventDefault();
        alert(this.state.oldpassword)
        if(this.state.newpassword === this.state.confirmpassword){
            axios.post(config.port+"/users/editprofile",{id:this.state.id,password:this.state.oldpassword,newpassword:this.state.newpassword}).then(res=>{
                
                this.setState({
                 modal: !this.state.modal,
        });
            })
        }

      }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }

        const profileinfo = this.state.profiledata.map((data,key)=>{
            return(
                <div>
                    <tr >
                    <th style={{width:"200px",margin:"8px"}}>User Name</th>
                    <td>{Capitalize(data.username)} </td>
                    </tr>
                    <tr>
                    <th>Registered</th>
                    <td><Moment format="YYYY/MM/DD">{data.ragistered}</Moment></td>
                    </tr>
                    <tr>
                    <th>Role</th>
                    <td>{Capitalize(data.role)} </td>
                    </tr>
                    <tr>
                    <th>Status</th>
                    <td>{Capitalize(data.status)} </td>
                    </tr>
                    
                </div>
            )
        })
if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col lg={4}>
                        <Card>
                        <CardHeader>
                            <h4> Profile Page  <Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} onClick={this.changepassword}>
                            <i className="fa fa-edit" style={{color:"black"}}></i></Button></h4>
                        </CardHeader>
                        <CardBody>
                            <Table borderless size="sm" striped>
                                <tbody>
                                {profileinfo}
                                </tbody>
                            </Table>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.modal} toggle={this.changepassword}
                       className={'modal-sm ' + this.props.className}>
                  <ModalHeader toggle={this.changepassword}>Change Password</ModalHeader>
                  <ModalBody>
                  <Form onSubmit={this.editprofile}>
                    <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="icon-lock"></i>
                                </InputGroupText>
                                </InputGroupAddon>
                                <Input type="password" placeholder="Old Password" name="oldpassword" onChange={this.myChangeHandler} />
                            </InputGroup>
                            <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="icon-lock"></i>
                                </InputGroupText>
                                </InputGroupAddon>
                                <Input type="password" placeholder="New Password" name="newpassword" onChange={this.myChangeHandler} />
                            </InputGroup>
                            <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="icon-lock"></i>
                                </InputGroupText>
                                </InputGroupAddon>
                                <Input type="password" placeholder="Confirm Password" name="confirmpassword" onChange={this.myChangeHandler} />
                            </InputGroup>
                            <Button color="primary" type="submit">Change</Button>
                       </Form>
                  </ModalBody>
                 
                </Modal>
            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default Profile;