import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import Employee from './employee';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <Employee match={{params: {id: "1"}, isExact: true, path: "/employee/:id", name: "employee details"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Admin</strong>)).toEqual(true)
  wrapper.unmount()
});