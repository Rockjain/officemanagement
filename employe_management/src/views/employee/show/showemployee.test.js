import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import ShowEmployee from './showemployee';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><ShowEmployee /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
