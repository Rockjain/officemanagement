import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Report from './report';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Report /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});