import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Form, Table,} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import config from '../../../port.js';

class AddSalary extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],emp_id:"0", hra:"", salary:"", transportation:"", medical:"", emp_pf_contri:"", empr_pf_contri:"",
    esi_of_emp_contri:"",esi_of_empr_contri:"",apply_date:"",status:"",redirect:false})
       
    }

    componentDidMount(){ 
      axios.get(config.port+"/users/currentemp").then(res=>{
          console.log(res.data)
          this.setState({employee:res.data})
      })
    }

      myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
       // console.log(this.state)
      }

      addsalary=(event)=>{
        event.preventDefault();
          console.log(this.state)
          if(this.state.emp_id === "0"){
            alert("Please Select Employee Name")
          }else{
          
                axios.post(config.port+"/users/addsalary",{
                    emp_id:this.state.emp_id,
                    apply_date:this.state.apply_date,
                    status:this.state.status,
                    basic_salary:this.state.salary,
                    hra:this.state.hra,
                    transportation_allowance:this.state.transportation,
                    medical_allowance:this.state.medical,
                    empr_pf_contri:this.state.empr_pf_contri,
                    emp_pf_contri:this.state.emp_pf_contri,
                    esi_of_empr_contri:this.state.esi_of_empr_contri,
                    esi_of_emp_contri:this.state.esi_of_emp_contri
                }).then(res=>{
                    if(res.data.success===true){
                        this.setState({redirect:true})
                    }
                })
         
        }
      }

      renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/salary' />
        }
      }

    render(){
      
if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4><i className="fa fa-inr"></i>&nbsp; Salary Setup</h4>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={this.addsalary}>
                           
                            <Table borderless>
                                <tbody>
                                    <tr>
                                        <th>Employee</th>
                                        <td>
                                            <select name="emp_id" id="emp_id" onChange={this.myChangeHandler} className="w-50" value={this.state.emp_id}>
                                                <option value="0">Please Select Employee</option>
                                                {
                                                this.state.employee.map((employee,key)=>{
                                                    return <option value={employee.id}>{employee.name}</option>
                                                })
                                                }
                                            </select>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <th>Salary</th>
                                        <td><input type="number" name="salary" className="w-50" placeholder="Salary" value={this.state.salary} onChange={this.myChangeHandler} required/></td>
                                    </tr>
                                    <tr>
                                        <th>Apply Date</th>
                                        <td><input type="date" name="apply_date" className="w-50" value={this.state.apply_date} onChange={this.myChangeHandler} /></td>
                                    </tr>

                                    <tr>
                                        <th>House Rent Allowance</th>
                                        <td><input type="number" name="hra" className="w-50" value={this.state.hra} placeholder="House Rent Allowance" onChange={this.myChangeHandler}/></td>
                                    </tr>
                                    <tr>
                                        <th>Transportation Allowance</th>
                                        <td><input type="number" name="transportation" value={this.state.transportation} className="w-50" placeholder="Transportation Allowance" onChange={this.myChangeHandler} /></td>
                                    </tr>

                                    <tr>
                                        <th>Medical Allowance</th>
                                        <td><input type="number" name="medical" className="w-50" value={this.state.medical} placeholder="Medical Allowance" onChange={this.myChangeHandler}/></td>
                                    </tr>

                                    <tr>
                                        <th>Employer PF Contribution</th>
                                        <td><input type="number" name="empr_pf_contri" value={this.state.empr_pf_contri} className="w-50" placeholder="Employer PF Contribution" onChange={this.myChangeHandler}/></td>
                                    </tr>

                                    <tr>
                                        <th>Employee PF Contribution</th>
                                        <td><input type="number" name="emp_pf_contri" className="w-50" value={this.state.emp_pf_contri} placeholder="Employee PF Contribution" onChange={this.myChangeHandler}/></td>
                                    </tr>

                                    <tr>
                                        <th>ESI of Employer Contribution</th>
                                        <td><input type="number" name="esi_of_empr_contri" className="w-50" value={this.state.esi_of_empr_contri} placeholder="ESI of Employer Contribution" onChange={this.myChangeHandler}/></td>
                                    </tr>

                                    <tr>
                                        <th>ESI of Employee Contribution</th>
                                        <td><input type="number" name="esi_of_emp_contri" className="w-50" placeholder="ESI of Employee Contribution" value={this.state.esi_of_emp_contri} onChange={this.myChangeHandler}/></td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Status</th>
                                        <td><select  name="status" id="status" className="w-50" onChange={this.myChangeHandler} required>
                                        <option value="0">Please Select Status</option>
                                        <option value="active">Active</option>
                                        <option value="deactive">Deactive</option>
                                        </select>
                                        </td>
                                    </tr>

                                </tbody>
                            </Table>
                            {this.renderRedirect()}
                                 <Button color="primary" className="float-right">Submit</Button>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default AddSalary;