/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button,Form, Input, InputGroup, InputGroupAddon, Badge, Table,} from 'reactstrap';
import {Link} from 'react-router-dom';
import { AppSwitch } from '@coreui/react';
import Moment from 'react-moment';
import Pagination from "react-js-pagination";
import config from '../../../port.js';

class Salary extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],emp_id:"0",checked:false,activePage: 1,pageSize:10,pageNumber:"",currentIndex:1,items:[],add:false,delete:true})
       
    }

      refreshItems=()=>{
        let items = this.state.employee.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
       // let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({items:items})
        console.log("item",this.state.items,this.state.pageSize)
      }

    componentDidMount(){ 
      axios.get(config.port+"/users/salary").then(res=>{
          console.log(res.data)
          this.setState({employee:res.data})
          this.refreshItems();
      })
    }

    handlePageChange=(pageNumber)=>{
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage:pageNumber,currentIndex:pageNumber});
        console.log("index",this.state.currentIndex)
       this.refreshItems();

       var ref = document.getElementById("salary")
                   
       var getSalary = ref.getElementsByClassName('d-check')
       for(let i=0;i<  getSalary.length;i++){
        getSalary[i].checked = false  
        }

      }

      myChangeHandler = (event) => {
        let val = event.target.value;
        console.log("val",val)
        this.setState({pageSize:val,currentIndex:1});
        this.refreshItems();
        console.log(this.state.pageSize)
    }

      deletesalary=(id)=>{
          axios.delete(config.port+"/users/deletesalary",{params:{id:id}}).then(res=>{
              console.log(res)
              if(res.data.success===true){
                axios.get(config.port+"/users/salary").then(res=>{
                    console.log(res.data)
                    this.setState({employee:res.data})
                    this.refreshItems();
                    var ref = document.getElementById("salary")
                   
                        var getSalary = ref.getElementsByClassName('d-check')
                        for(let i=0;i<  getSalary.length;i++){
                            getSalary[i].checked = false  
                            }

                     })
              }
          })
      }

      multipledelete=()=>{
        var ref = document.getElementById("salary")
        let ids=[];
         var getSalary = ref.getElementsByClassName('d-check')
         for(let i=0;i<getSalary.length;i++){
             
             if(getSalary[i].checked === true){
                 let id={id:getSalary[i].id}
                 ids.push(id);
               }
              
         }
         console.log(ids)
         let tid = JSON.stringify(ids)
         axios.delete(config.port+"/users/deletemultiplesalary",{params:{ids:tid}}).then(res=>{
             if(res.data.success === true){
                 for(let i=0;i<getSalary.length;i++){
                     getSalary[i].checked = false  
                 }
 
                 this.setState({add:false,delete:true})
                 axios.get(config.port+"/users/salary").then(res=>{
                    console.log(res.data)
                    this.setState({employee:res.data})
                }) 
             
             }
         })
    }

      handleCheck=(emp_id,id)=>{
        axios.post(config.port+"/users/toggleStatus",{id:id,emp_id:emp_id}).then(res=>{
            console.log(res.data)
            if(res.data.success===true){
                axios.get(config.port+"/users/salary").then(res=>{
                    console.log(res.data)
                    this.setState({employee:res.data})
                    this.refreshItems();
                })
            }
        })
      }

        onchangecheckbox=()=>{
        var ref = document.getElementById("salary")
        let checkdepartment =0
        var getsalary = ref.getElementsByClassName('d-check')
        for(let i=0;i<getsalary.length;i++){
            
            if(getsalary[i].checked === true){
                checkdepartment++
                
              }
             
        }
        if(checkdepartment >0){
            this.setState({add:true,delete:false}) 
        }else{
            this.setState({add:false,delete:true})
        }
    }


      search=(event)=>{
        let val = event.target.value;
 
        var queryResult=[];
        if(val !== ""){
        axios.get(config.port+"/users/searchsalary",{params:{val:val}}).then(res=>{
      
            res.data.map((rows,index)=>{
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({employee:queryResult})
        })
        }else{
            this.refreshItems();
        }
      }

    render(){
        const getBadge = (status) => {
            return status === 'active' ? 'primary' :
                status === 'deactive' ? 'warning' :
                    'primary'
                }
        
        const salaryBadge =(status)=>{
            return status === 'active' ? 'primary' :
                status === 'deactive' ? 'light' :
                    'primary'
        }

         const Capitalize=(str)=>{
            return str.charAt(0).toUpperCase() + str.slice(1);
            }

            
        const paginationContent = (
            <div className="ml-5">
             <Pagination
                hideDisabled
                prevPageText='prev'
                nextPageText='next'
                firstPageText='first'
                lastPageText='last'
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.pageSize}
                totalItemsCount={this.state.employee.length}
                itemClass="page-item"
                linkClass="page-link"
                onChange={this.handlePageChange}
                />
              </div>
          )
    
          var pagination;
          if(this.state.employee.length > this.state.pageSize){
            pagination = <div>{paginationContent}</div>
          }else{
            pagination = <div></div>
          }
      
if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4><i className="fa fa-inr"></i>&nbsp; Salary Setup
                            <Link to='/salary/add'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}>
                            <i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
                            <a className="btn btn-danger btn-sm ml-2 float-right" hidden={this.state.delete} onClick={this.multipledelete}><i className="fa fa-trash" style={{color:"white"}} ></i></a>
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                                        <InputGroup className="mb-2">
                                                                <InputGroupAddon addonType="prepend"><Button><i className="fa fa-search"></i></Button></InputGroupAddon>
                                                                <Input placeholder="Search" className="w-75" name="searchbar" onChange={this.search}/> 
                                                                <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler} >
                                                                    <option value="10">10</option>
                                                                    {this.state.employee.length>10?<option value="15">15</option>:<span> </span>}
                                                                    {this.state.employee.length>15?<option value="20">20</option>:<span> </span>}
                                                                    {this.state.employee.length>20?<option value="25">25</option>:<span> </span>}
                                                                    {this.state.employee.length>25?<option value="30">30</option>:<span> </span>}
                                                                    </Input>
                                                            </InputGroup>
                                                        
                                                        </div>
                            <Form id="salary">
                            <Table>
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Employee Name</th>
                                    <th>Employee Code</th>
                                    <th>Gender</th>
                                    <th>Salary</th>
                                    <th>Apply Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {this.state.items.map((emp,index)=>{
                                        const editLink = `salary/edit/${emp.id}` 
                                        var checked;
                                        emp.status ==="active"? checked=true:checked=false;
                                        return(
                                            <tr key={index}>
                                                <td><input type="checkbox" className="mr-2 d-check" value={emp.id} id={emp.id}  onChange={this.onchangecheckbox}/>
                                                {index+1}</td>
                                                <td>{Capitalize(emp.emp_name)}</td>
                                                <td>{emp.emp_code}</td>
                                                <td>{Capitalize(emp.gender)}</td>
                                                <td><Badge color={salaryBadge(emp.status)}>{emp.basic_salary}</Badge></td>
                                                <td><Moment format="DD-MM-YYYY">{emp.apply_date}</Moment> </td>
                                                <td>
                                                <Badge color={getBadge(emp.status)}>{Capitalize(emp.status)}</Badge>
                                               
                                                <AppSwitch className={'mx-1'} variant={'3d'} color={'primary'} checked={checked} label dataOn={'\u2713'} dataOff={'\u2715'} onChange={()=>this.handleCheck(emp.emp_id,emp.id)}/>
                                                </td>

                                                <td>
                                                     <Link to={editLink}><Button className="btn-sm" color="primary"><i className="fa fa-pencil" style={{color:"white"}}></i></Button></Link>

                                              <a className="btn btn-danger btn-sm ml-2" onClick={()=>this.deletesalary(emp.id)}><i className="fa fa-trash" style={{color:"white"}}></i></a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </Table>
                            <div className="ml-5">
                                                      {pagination}
                                                    </div>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default Salary;