import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddSalary from './addsalary';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddSalary /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});