/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button,Form,Table,} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import config from '../../../port.js';


class GenerateSalary extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],emp_id:[],emp_code:[],basic_salary:[],hra:[],transportation_allowance:[],medical_allowance:[],
           empr_pf_contri:[],emp_pf_contri:[],esi_of_empr_contri:[],esi_of_emp_contri:[],bonus:[],advance:[],tds:[],absent:[],
           status:[], startDate:"",days:"",total_salary:[], paid_date:[],redirect:false,update:true, submit:false,message:"",
           absent_short_time:[]})
       
    }

    componentDidMount(){ 
        let date = new Date();

        let year = date.getFullYear();
        let month = date.getMonth()
        let days = this.daysInMonth(month+1,year)
        console.log(days,month)
        this.setState({startDate:date,days:days})
        axios.get(config.port+"/users/activesalary").then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})

            let total_salary=[];
            let bonus=[];
            let advance=[];
            let tds=[];
            let absent=[];
            let emp_id=[];
            let emp_code=[];
            let basic_salary=[];
            let hra=[];
            let transportation_allowance=[];
            let medical_allowance=[];
            let empr_pf_contri=[];
            let emp_pf_contri=[];
            let esi_of_emp_contri=[];
            let esi_of_empr_contri=[];
            let status=[];
            let paid_date=[];
            let absent_short_time=[];
            res.data.map((data,key)=>{

            emp_id.push(data.emp_id);
            emp_code.push(data.emp_code);
            basic_salary.push(data.basic_salary);
            hra.push(data.hra);
            transportation_allowance.push(data.transportation_allowance);
            medical_allowance.push(data.medical_allowance);
            empr_pf_contri.push(data.empr_pf_contri);
            emp_pf_contri.push(data.emp_pf_contri);
            esi_of_emp_contri.push(data.esi_of_emp_contri);
            esi_of_empr_contri.push(data.esi_of_empr_contri);

            let salary = parseInt(data.basic_salary)+parseInt(data.hra)+parseInt(data.transportation_allowance)+parseInt(data.medical_allowance)-parseInt(data.empr_pf_contri)-parseInt(data.emp_pf_contri)-parseInt(data.esi_of_empr_contri)-parseInt(data.esi_of_emp_contri);

            total_salary.push(salary);
            bonus.push(0.00);
            advance.push(0.00);
            tds.push(0.00);
            absent.push(0.00);
            status.push("");
            paid_date.push("");
            absent_short_time.push(0.00);
            }
            );

            this.setState({total_salary:total_salary, bonus:bonus, advance:advance, tds:tds, absent:absent, status:status, 
            emp_id:emp_id, emp_code:emp_code, basic_salary:basic_salary, hra:hra, transportation_allowance:transportation_allowance,
            medical_allowance:medical_allowance, empr_pf_contri:empr_pf_contri, emp_pf_contri:emp_pf_contri, esi_of_empr_contri:esi_of_empr_contri, esi_of_emp_contri:esi_of_emp_contri, paid_date:paid_date , absent_short_time:absent_short_time})
            console.log(this.state)
        })
    }
   
    daysInMonth=(month, year)=>{
        return new Date(year, month, 0).getDate();
    }

    handleChangeStart=(date)=>{
    let month = date.getMonth()+1;
    let year = date.getFullYear();
    let days = this.daysInMonth(month+1,year)
    console.log("days",month,year)
        this.setState({
            startDate: date,days:days
          });
    }

    handlebonusChange = idx => evt => {
        const newbonus = this.state.bonus.map((exprience, sidx) => {
          if (idx === sidx){
                if(evt.key === "Enter"){
                if(evt.target.value !== ""){
                    const total = this.state.total_salary.map((salary,sidx)=>{
                        if(idx !== sidx) return salary;
                        return parseInt(evt.target.value)+parseInt(salary)
                    })
                    
                    this.setState({total_salary:total});
                    }
                }
              return  evt.target.value 
            }else{
                return exprience;
            }
         
        });
       
        this.setState({ bonus: newbonus});
      }

      handleadvanceChange = idx=>evt=>{
        const advance = this.state.advance.map((advance, sidx) => {
            if (idx === sidx){
                  if(evt.key === "Enter"){
                  if(evt.target.value !== ""){
                      const total = this.state.total_salary.map((salary,sidx)=>{
                          if(idx !== sidx) return salary;
                          return parseInt(salary)- parseInt(evt.target.value)
                      })
                      
                      this.setState({total_salary:total});
                      }
                     
                  }
                  return evt.target.value
              }else{
                  return advance;
              }
           
          });
         
          this.setState({advance:advance});
          console.log(this.state.advance)
      }

      handletdsChange = idx=>evt=>{
        const tds = this.state.tds.map((tds, sidx) => {
            if (idx === sidx){
                  if(evt.key === "Enter"){
                  if(evt.target.value !== ""){
                      const total = this.state.total_salary.map((salary,sidx)=>{
                          if(idx !== sidx) return salary;
                          return parseInt(salary)-parseInt(evt.target.value)
                      })
                      
                      this.setState({total_salary:total});
                      }
                  }
                return  evt.target.value 
              }else{
                  return tds;
              }
           
          });
         
          this.setState({ tds: tds});
      }

        salarydays=idx=>evt=>{
        const absent_days = this.state.absent.map((days, sidx) => {
            if (idx === sidx) return evt.target.value ;
            return  days 
          });
         
          this.setState({ absent: absent_days});
        }

        enterPressed =idx=>evt=>{
           console.log(evt.key,idx)
           
            const absent_days = this.state.absent.map((days, sidx) => {
                if (idx === sidx){

                    if(evt.target.value !== ""){
                        if(evt.key === "Enter"){
                            console.log("absent",evt.target.value)
                            let salarydays = parseInt(this.state.days)-parseInt(evt.target.value);
                            console.log(salarydays)
                                const total = this.state.total_salary.map((salary,sidx)=>{
                                if(idx === sidx){
                                    let sal =  (parseInt(salarydays)*parseInt(salary))/parseInt(this.state.days)
                                    let totalsalary = Math.round(sal)
                                   
                                    const absent_short_time =this.state.absent_short_time.map((time,sidx)=>{
                                        if (idx === sidx){
                                       let absent_short_time  = (parseInt(evt.target.value)*parseInt(salary))/parseInt(this.state.days)

                                       let absent_shorttime = Math.round(absent_short_time)
                                        return absent_shorttime;
                                        }else{
                                            return time;
                                        }
                                       })

                                    this.setState({absent_short_time:absent_short_time})
                                    return totalsalary;
                                    
                                }else{
                                    return salary;
                                }
                                
                            })
                            this.setState({total_salary:total});
                        }
                    }

                    return  evt.target.value;
                    }else{

                    return days;
                    }
              });
              
             
              this.setState({ absent: absent_days});
              console.log(this.state.days,absent_days, this.state.absent_short_time)
           
        }

        handelstatuschange=idx=>evt=>{
        const status = this.state.status.map((status, sidx) => {
            if (idx === sidx){
              const date = this.state.paid_date.map((date,sidx)=>{
                if(idx === sidx){
                        return new Date();
                }else{
                    return date;
                }
              })
                this.setState({paid_date:date})
                return evt.target.value ;
                }else{
                    return status  
                }            
          });
         
          this.setState({status:status});
          console.log(this.state.status, this.state.paid_date)
        }

        submitTransaction=(event)=>{
            event.preventDefault();
            console.log(this.state.status, this.state.paid_date)
            
            for(let i=0; i<this.state.employee.length; i++){
                console.log(this.state.emp_id[i])
                if(this.state.status[i] === ""){
                    this.setState({message:"Please Select the Status"})
                }else{
                    axios.post(config.port+"/users/addtransaction",{
                        emp_id:this.state.emp_id[i],
                        emp_code:this.state.emp_code[i],
                        paid_date:this.state.paid_date[i],
                        paid_status:this.state.status[i],
                        total_paid:this.state.total_salary[i],
                        basic_salary:this.state.basic_salary[i],
                        hra:this.state.hra[i],
                        transportation_allowance:this.state.transportation_allowance[i],
                        medical_allowance:this.state.medical_allowance[i],
                        bonus:this.state.bonus[i],
                        empr_pf_contri:this.state.empr_pf_contri[i],
                        emp_pf_contri:this.state.emp_pf_contri[i],
                        esi_of_empr_contri:this.state.esi_of_empr_contri[i],
                        esi_of_emp_contri:this.state.esi_of_emp_contri[i],
                        advance:this.state.advance[i],
                        tds:this.state.tds[i],
                        absent_short_time:this.state.absent_short_time[i]
                    }).then(res=>{
                        console.log(res.data);
                       if(res.data.success===true){
                           this.setState({redirect:true})
                       }else{
                           this.setState({update:false,submit:true,
                        message:"Data already exist of this month if you want to update then click on update button."})
                       }
                    })
            }
        }
           
        }

        updateTransaction=(event)=>{
            event.preventDefault();
            for(let i=0; i<this.state.employee.length; i++){
                console.log(this.state.emp_id[i])
                    axios.post(config.port+"/users/updatetransaction",{
                        emp_id:this.state.emp_id[i],
                        paid_date:this.state.paid_date[i],
                        paid_status:this.state.status[i],
                        total_paid:this.state.total_salary[i],
                      
                        bonus:this.state.bonus[i],
                        advance:this.state.advance[i],
                        tds:this.state.tds[i],
                        absent_short_time:this.state.absent_short_time[i]

                    }).then(res=>{
                        console.log(res.data);
                       if(res.data.success===true){
                           this.setState({redirect:true})
                       }
                    })
            }
        }

        renderRedirect = () => {
            if (this.state.redirect) {
              return <Redirect to='/transaction' />
            }
          }

    render(){

         const Capitalize=(str)=>{
            return str.charAt(0).toUpperCase() + str.slice(1);
            }

      
if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4><i className="fa fa-inr"></i>&nbsp; Generate Salary
                            <span className="float-right"> <DatePicker
                                    selected={this.state.startDate}
                                    selectsStart
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    dateFormat="MMMM, yyyy"
                                    showMonthYearPicker
                                    onChange={this.handleChangeStart}
                                    className="w-50 pl-1 float-right"/> </span>
                            </h4>
                           
                        </CardHeader>
                        <CardBody>
                            <Form >
                                <div style={{float:"right"}}>
                               
                                </div>
                            <div class="table-responsive">
                                <Table>
                                
                                <thead>
                                    <tr >
                                        <th className="align-top">Id</th>
                                        <th className="align-top" style={{width:"350px!important"}}>Employee Name</th>
                                       
                                        <th className="align-top">Bonus</th>
                                        <th className="align-top">Advance</th>
                                        <th className="align-top">TDS</th>
                                        <th className="align-top">Absent(No. of days)</th>
                                        <th className="align-top">Salary Paid</th>
                                        <th className="align-top">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.employee.map((emp,key)=>{
                                        let absent = `absent-${key}`
                                        let bonus = `bonus-${key}`
                                        let advance = `advance-${key}`
                                        let tds=`tds-${key}`
                                        let gender = `gender-${key}`
                                        return(
                                            <tr key={key}>
                                            <td>{emp.id}</td>
                                            <td>{Capitalize(emp.emp_name)} </td>
                                           

                                            <td><input type="text" name={bonus} value={this.state.bonus[key]} onChange={this.handlebonusChange(key)} onKeyPress={this.handlebonusChange(key)} className="w-100"/></td>

                                            <td><input type="text" name={advance} value={this.state.advance[key]}  onChange={this.handleadvanceChange(key)} onKeyPress={this.handleadvanceChange(key)} className="w-100"/></td>

                                            <td width="100px"><input type="text" name={tds} value={this.state.tds[key]}  onChange={this.handletdsChange(key)} onKeyPress={this.handletdsChange(key)} className="w-100"/></td>

                                            <td><input type="text" name={absent} value={this.state.absent[key]} onChange={this.salarydays(key)} onKeyPress={this.enterPressed(key)} className="w-100"/></td>

                                            <td><input type="text" name="totalsalary" value={this.state.total_salary[key]}/></td>
                                            <td  width="150px">
                                                <input type="radio" name={gender} onChange={this.handelstatuschange(key)} value="paid" onClick={this.handelstatuschange(key)} required/> Paid
                                                &nbsp;
                                                <input type="radio" name={gender} onChange={this.handelstatuschange(key)}  value="unpaid" required /> Unpaid
                                               
                                            </td>
                                            </tr>
                                        )
                                    })}
                                    <tr><td colspan="6">
                                    <h6 className="text-warning">{this.state.message}</h6>
                                    </td>
                                    {this.renderRedirect()}
                                    <td colspan="2">
                                    <Button color="primary" type="button" className="float-right" onClick={this.submitTransaction}
                                    hidden={this.state.submit}>Submit</Button>
                                    <Button color="primary" type="button" className="float-right" onClick={this.updateTransaction} hidden={this.state.update}>Update</Button>
                                    </td></tr>
                                </tbody>
                                
                                </Table>
                               
                            </div>
                           
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default GenerateSalary;