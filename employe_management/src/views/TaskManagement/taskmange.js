import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Link } from 'react-router-dom'
import Moment from 'react-moment';
import config from '../../port.js';

class TaskManagement extends Component{
    constructor(props){
        super(props)
        this.state = ({client:[],add:false})
       
    }

    componentDidMount(){ 
     
    }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Task Management
                            <Link to='/taskmanage/add'>

                            <Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}><i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <Table>
                                
                            </Table>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default TaskManagement;