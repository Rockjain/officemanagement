/* eslint-disable array-callback-return */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button,Table} from 'reactstrap';
import html2pdf from 'html2pdf.js';
import config from '../../port.js';

class FinalReport extends Component{
    constructor(props){
        super(props)
        this.state = ({employe:[],salary:[], fromyear:[], toyear:[], fromdate:[], todate:[]})
       
    }

    componentDidMount(){ 
        let date = new Date()
        let year = date.getFullYear();
        let fromdate = new Date(year, 3, 1)
        let todate = new Date(year+1 , 3, 1)
        
        this.setState({fromdate:fromdate, todate:todate, fromyear:year, toyear:year+1})
       axios.get(config.port+"/users/finalreport",{params:{fromdate:fromdate, todate:todate}}).then(res=>{
           console.log(res.data)

           let salary=[];
           res.data.map((item,index)=>{
               if(index === 0){
                   item.paid_detail =[{
                    paid_date:item.paid_date,
                    total_paid:Math.round(item.total_paid),
                    paid_month:item.paid_month
                   }]

                   salary.push(item)
               }
               else{
                let itemfound = false;
                let itemindex;
               for(let i=0; i<salary.length; i++){
                if(salary[i].emp_id === item.emp_id){
                  itemfound = true;
                  itemindex = i;
                  console.log(itemfound)
                  break;
                }
               }

                
    if(itemfound){
     
        //console.log(itemindex)
        console.log("salary",salary)
    
              salary[itemindex].paid_detail.push({ paid_date:item.paid_date,total_paid:Math.round(item.total_paid), paid_month:item.paid_month})
       
      }else{
        item.paid_detail=[{
            paid_date:item.paid_date,
            total_paid:Math.round(item.total_paid),
            paid_month:item.paid_month
        }]
       
            salary.push(item)
            }
        }
      })
           console.log(salary)
           this.setState({salary:salary})
       })
    }

      myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      }

      prev=()=>{
        let fromyear = this.state.fromyear-1;
        let toyear = this.state.toyear-1;
        console.log("fromto", fromyear, toyear)
        this.setState({fromyear:fromyear, toyear:toyear})

        let fromdate = new Date(fromyear, 3, 1)
        let todate = new Date(toyear , 3, 1)
        axios.get(config.port+"/users/finalreport",{params:{fromdate:fromdate, todate:todate}}).then(res=>{
            console.log(res.data)
 
            let salary=[];
            res.data.map((item,index)=>{
                if(index === 0){
                    item.paid_detail =[{
                     paid_date:item.paid_date,
                     total_paid:item.total_paid,
                     paid_month:item.paid_month
                    }]
 
                    salary.push(item)
                }
                else{
                 let itemfound = false;
                 let itemindex;
                for(let i=0; i<salary.length; i++){
                 if(salary[i].emp_id === item.emp_id){
                   itemfound = true;
                   itemindex = i;
                   console.log(itemfound)
                   break;
                 }
                }
 
                 
     if(itemfound){
      
         //console.log(itemindex)
         console.log("salary",salary)
     
               salary[itemindex].paid_detail.push({ paid_date:item.paid_date,total_paid:item.total_paid, paid_month:item.paid_month})
        
       }else{
         item.paid_detail=[{
             paid_date:item.paid_date,
             total_paid:item.total_paid,
             paid_month:item.paid_month
         }]
        
             salary.push(item)
             }
         }
       })
            console.log(salary)
            this.setState({salary:salary})
        })
      }
      next=()=>{
        let fromyear = this.state.fromyear+1;
        let toyear = this.state.toyear+1;
        console.log("fromto", fromyear, toyear)
        this.setState({fromyear:fromyear, toyear:toyear})

        
        let fromdate = new Date(fromyear, 3, 1)
        let todate = new Date(toyear , 3, 1)
        axios.get(config.port+"/users/finalreport",{params:{fromdate:fromdate, todate:todate}}).then(res=>{
            console.log(res.data)
 
            let salary=[];
            res.data.map((item,index)=>{
                if(index === 0){
                    item.paid_detail =[{
                     paid_date:item.paid_date,
                     total_paid:item.total_paid,
                     paid_month:item.paid_month
                    }]
 
                    salary.push(item)
                }
                else{
                 let itemfound = false;
                 let itemindex;
                for(let i=0; i<salary.length; i++){
                 if(salary[i].emp_id === item.emp_id){
                   itemfound = true;
                   itemindex = i;
                   console.log(itemfound)
                   break;
                 }
                }
 
                 
     if(itemfound){
      
         //console.log(itemindex)
         console.log("salary",salary)
     
               salary[itemindex].paid_detail.push({ paid_date:item.paid_date,total_paid:item.total_paid, paid_month:item.paid_month})
        
       }else{
         item.paid_detail=[{
             paid_date:item.paid_date,
             total_paid:item.total_paid,
             paid_month:item.paid_month
         }]
        
             salary.push(item)
             }
         }
       })
            console.log(salary)
            this.setState({salary:salary})
        })
      }

      pdf=()=>{
        const input = document.getElementById('pdf');
        
        var opt= {
            margin:[0,2,1,3],
            filename:'report.pdf',
            jsPDF:{ format: 'letter', orientation: 'landscape' }
           
        };

         html2pdf().set(opt).from(input).save();
      }

    render(){
       const employedata = this.state.salary.map((salary,key)=>{
           return(
             
                        <tr>
                        <td>{salary.name}</td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 4)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 5)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 6)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 7)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 8)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 9)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 10)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 11)
                            return data.total_paid
                        })}
                        </td>

                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 12)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 1)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 2)
                            return data.total_paid
                        })}
                        </td>
                        <td>
                        {salary.paid_detail.map((data,key)=>{
                            if(data.paid_month === 3)
                            return data.total_paid
                        })}
                        </td>
                        </tr>
               
           )
       })
     
    if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4><Button color="secondary" size="sm" className="mr-2" type="button" onClick={this.prev}>Prev</Button> 
                            {this.state.fromyear}-{this.state.toyear}  
                            <Button color="secondary" size="sm" className="ml-2" type="button" onClick={this.next}>Next</Button>
                            <Button color="danger" className="float-right" type="button" onClick={this.pdf}><i class="fa fa-download"></i> Download</Button></h4>
                        </CardHeader>
                        <CardBody>
                            <div id="pdf" className="mt-2 mb-3">
                            <Table bordered size="sm" responsive>
                            <thead> 
                                <tr>
                                    <th>Name</th>
                                    <th>April</th>
                                    <th>May </th>
                                    <th>June</th>
                                    <th>July</th>
                                    <th>August</th>
                                    <th>September</th>
                                    <th>October</th>
                                    <th>November</th>
                                    <th>December</th>
                                    <th>January</th>
                                    <th>February</th>
                                    <th>March</th>
                                </tr>
                            </thead>
                            <tbody>
                                {employedata}
                                </tbody>
                            </Table>
                           </div>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default FinalReport;