import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import axios from 'axios';
import Moment from 'react-moment';
import config from '../../port.js';

class User extends Component {
  constructor(props){
    super(props); 
    this.state = ({admin:[]});
  }

  componentDidMount(){
    axios.get(config.port+"/users/admin").then(res=>{
      console.log("data",res.data)
      this.setState({admin:res.data})
      console.log(this.state.admin)
    })
  }

  render() {
    
    const user = this.state.admin.find( user => user.id.toString() === this.props.match.params.id)

    const userDetails = user ? Object.entries(user) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]

    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong><i className="icon-info pr-1"></i>User id: {this.props.match.params.id}</strong>
              </CardHeader>
              <CardBody>
                  <Table responsive striped hover>
                    <tbody>
                      {
                        userDetails.map(([key, value]) => {
                          return (
                            <tr key={key}>
                              <td>{`${key}:`}</td>
                             
                          <td> {key === "ragistered" ?<strong><Moment format="YYYY/MM/DD">{value}</Moment></strong>: <strong>{value}</strong>}</td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default User;
