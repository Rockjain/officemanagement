import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom'
import Moment from 'react-moment';
import config from '../../port.js';
import './project.css';


class EditProject extends Component{
    constructor(props){
        super(props)
        this.state = ({projects:[], client:[], add:false, name:"",client_id:"", client_name:"",technology:[], project_tech:"", start_date:" ", end_date:"", status:"", cost:"", project_from:"", image:"", details:" ", deadline:" ", type:" ",multerImage:"",file:[],discription:"",project_type:"", redirect:false, images:[]})
       
    }

    componentDidMount(){ 
    let id = this.props.match.params.id;
    axios.get(config.port+"/users/showclient").then(res=>{
        console.log(res.data)
        this.setState({client:res.data})
    })

    axios.get(config.port+"/users/projectTech").then(res=>{
        console.log(res.data)
        this.setState({technology:res.data})
    })

    axios.get(config.port+"/users/projectbyid",{params:{id:id}}).then(res=>{
        console.log(res.data)
        let data=res.data.rows[0]
        let doc = res.data.document.file
        let img = res.data.document.images
        let document=[];
        let images=[];
        for(var i in doc){
            document.push(doc[i])
        }
        for(var j in img){
            images.push(img[j])
        }
        this.setState({name:data.name,client_id:data.client_id, client_name:data.client_name, project_tech:data.project_tech, start_date:data.start_date, end_date:data.end_date, status:data.status, cost:data.cost, project_from:data.project_from, deadline:data.deadline, project_type:data.project_type_id, discription:res.data.document.discription, file:document, images:images})
    })
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
  
    }

    upload=(e)=>{
       
        console.log(e.target.files[0].type)
        let document=[];
        let images=[];
        const formData = new FormData();
        this.state.file.map((data,key)=>{
            document.push(data)
        })
        this.state.images.map((data,key)=>{
            images.push(data)
        })
        for(var x = 0; x<e.target.files.length; x++) {
        formData.append('myImage',e.target.files[x]);
        if(e.target.files[x].type === "image/png" || e.target.files[x].type === "image/jpg" || e.target.files[x].type === "image/jpeg"){
            images.push(e.target.files[x].name)
        }
        else document.push(e.target.files[x].name)
        }

        console.log(document,images);
        this.setState({file:document,images:images})
        axios.post(config.port+"/users/upload",formData,{
        }).then((res) => {
            console.log(res.statusText)
            }).catch((error) => {
        });
    }


    update=(e)=>{
        e.preventDefault();
        console.log(this.state)
        axios.post(config.port+"/users/updateproject",{
            name:this.state.name,
            client_id:this.state.client_id,
            project_tech:this.state.project_tech,
            cost:this.state.cost,
            project_from:this.state.project_from,
            file:this.state.file,
            images:this.state.images,
            discription:this.state.discription,
            deadline:this.state.deadline,
            end_date:this.state.end_date,
            project_type_id:this.state.project_type,
            start_date:this.state.start_date,
            status:this.state.status,
            id:this.props.match.params.id
        }).then(res=>{
            if(res.data.success === true){
                this.setState({redirect:true})
            }
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/project' />
        }
      }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Edit Project</h4>
                        </CardHeader>
                        <CardBody>
                        <Form enctype = "multipart/form-data" onSubmit={this.update}>
                            <Table borderless>
                                <tr>
                                    <th>Project Name :</th>
                                    <td><input type="input" name="name" value={this.state.name} placeholder="Project name" className="w-75 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Client Name :</th>
                                    <td><select name="client_id" value={this.state.client_id}  id="client_id" className="p-1 w-50" onChange={this.myChangeHandler}>
                                    <option value="0">Please Select Client</option>
                                    {this.state.client.map((client,key)=>{
                                        return(
                                            <option value={client.id}>{client.name}</option>
                                        )
                                    })}
                                    </select> </td>
                                </tr>

                                <tr>
                                    <th>Project Technology :</th>
                                    <td><input type="input" name="project_tech" value={this.state.project_tech}  placeholder="Project Technology" className="w-75 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>

                                <tr>
                                    <th>Start Date : </th>
                                    <td><input type="date" name="start_date" value={this.state.start_date} className="w-50 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>

                                <tr>
                                    <th>End Date : </th>
                                    <td><input type="date" name="end_date" value={this.state.end_date} className="w-50 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>

                                <tr>
                                    <th>Cost : </th>
                                    <td><input type="input" name="cost" value={this.state.cost}  placeholder="Cost" className="w-75 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>

                                <tr>
                                    <th>Project From : </th>
                                    <td><input type="input" name="project_from" value={this.state.project_from}  className="w-75 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>

                                <tr>
                                    <th>Project Type :</th>
                                    <td>
                                    <select name="project_type" id="project_type" value={this.state.project_type}  className="p-1 w-50" onChange={this.myChangeHandler}>
                                    <option value="0">Please Select Project Type</option>
                                    {this.state.technology.map((project,key)=>{
                                        return(
                                            <option value={project.id}>{project.type}</option>
                                        )
                                    })}
                                    </select>   
                                    </td>
                                </tr>

                                <tr>
                                    <th>Status :</th>
                                    <td>
                                {this.state.status === "not_started"?<input type="radio" name="status" value="not_started" onChange={this.myChangeHandler} checked/>:<input type="radio" name="status" value="not_started" onChange={this.myChangeHandler}/>} Not Started 

                                   {this.state.status === "in_progress"?<input type="radio" name="status" className="ml-3" value="in_progress" onChange={this.myChangeHandler} checked/>:<input type="radio" name="status" className="ml-3" value="in_progress" onChange={this.myChangeHandler}/>} In Progress
                                    {this.state.status === "completed" ?<input type="radio" name="status" className="ml-3" value="completed" onChange={this.myChangeHandler} checked/>:<input type="radio" name="status" className="ml-3" value="completed" onChange={this.myChangeHandler}/>} Completed
                                    </td>
                                </tr>

                                <tr>
                                    <th>Deadline :</th>
                                    <td><input type="date" name="deadline" value={this.state.deadline} className="w-50 p-1" onChange={this.myChangeHandler}/></td>
                                </tr>

                                <tr>
                                    <th>Project Document :</th>
                                    <td><input type="file" name="myImage"  multiple onChange={this.upload}/><br /><br />
                                        <Input type="textarea" name="discription" placeholder="discription" value={this.state.discription} className="w-75" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    {this.renderRedirect()}
                                    <td colspan="2"><Button type="submit" color="primary">Update</Button></td>
                                </tr>
                            </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default EditProject;