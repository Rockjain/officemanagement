import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import EditProject from './edit';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <EditProject match={{params: {id: "1"}, isExact: true, path: "/project/edit/:id", name: "Edit Project"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Project</strong>)).toEqual(true)
  wrapper.unmount()
});