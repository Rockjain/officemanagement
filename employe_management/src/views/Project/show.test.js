import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import ShowProject from './show';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <ShowProject match={{params: {id: "1"}, isExact: true, path: "/project/show/:id", name: "Show Project"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Project</strong>)).toEqual(true)
  wrapper.unmount()
});