import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Link } from 'react-router-dom'
import Moment from 'react-moment';
import config from '../../port.js';
import './project.css';


class ShowProject extends Component{
    constructor(props){
        super(props)
        this.state = ({projects:[],document:[],discription:"",modal: false,imagesrc:"",images:[]})
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount(){ 
    let id = this.props.match.params.id;
    axios.get(config.port+"/users/projectbyid",{params:{id:id}}).then(res=>{
        console.log(res.data)
        let doc = res.data.document.file
        let img = res.data.document.images
        let document=[];
        let images=[];
        for(var i in doc){
            document.push(doc[i])
        }
        for(var j in img){
            images.push(img[j])
        }
        console.log(document)
        this.setState({projects:res.data.rows, document:document, discription:res.data.document.discription, images:images})
    })
    }

    toggle=(imagesrc)=>{
        this.setState(prevState => ({
          modal: !prevState.modal,imagesrc:imagesrc
        }));
      }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Project Detail
                            <Link to={"/project/edit/"+this.props.match.params.id} style={{color:"black"}}><i className="fa fa-pencil float-right"></i></Link>
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <div className="row">
                             
                            <div className="col-md-12 col-lg-8">
                                <Table borderless striped>
                                    {this.state.projects.map((project,key)=>{
                                        return(
                                            <tbody>
                                                <tr>
                                                    <th>Project Name</th>
                                                    <td>{project.name}</td>
                                                </tr>
                                                <tr>
                                                    <th>Client Name</th>
                                                    <td>{project.client_name} </td>
                                                </tr>
                                                <tr>
                                                    <th>Technology</th>
                                                    <td>{project.project_tech} </td>
                                                </tr>
                                                <tr>
                                                    <th>Start Date</th>
                                                    <td><Moment format="DD-MMMM-YYYY">{project.start_date}</Moment></td>
                                                </tr>
                                                <tr>
                                                    <th>Deadline</th>
                                                    <td><Moment format="DD-MMMM-YYYY">{project.deadline}</Moment></td>
                                                </tr>
                                                <tr>
                                                    <th>End Date</th>
                                                    <td>{project.end_date === null ?"":<Moment format="DD-MMMM-YYYY">{project.end_date}</Moment>}</td>
                                                </tr>
                                                <tr>
                                                    <th>Status</th>
                                                    <td>{project.status} </td>
                                                </tr>
                                                <tr>
                                                    <th>Cost</th>
                                                    <td>{project.cost} </td>
                                                </tr>
                                                <tr>
                                                    <th>Project From</th>
                                                    <td>{project.project_from} </td>
                                                </tr>
                                                <tr>
                                                    <th>Discription</th>
                                                    <td>{this.state.discription} </td>
                                                </tr>
                                                <tr>
                                                    <th>Document</th>
                                                    <td>
                                                    {this.state.document.map((data,key)=>{
                                                         let docsrc = `${config.port}/images/${data}`
                                                        return <a target="blank" href={docsrc}>{data +","} &nbsp;</a>
                                                    })}
                                                     
                                                    </td>
                                                </tr>
                                            </tbody>
                                        )
                                    })}
                                </Table>
                               </div>
                               <div className="col-md-12 col-lg-4">
                               {this.state.images.map((doc,key)=>{
                                    let imagesrc = `${config.port}/images/${doc}`
                                   return(
                                       <div>
                                        <img src={imagesrc} alt="" className="m-2" width="80%" onClick={()=>this.toggle(imagesrc)}/> 

                                            <Modal isOpen={this.state.modal} fade={false} toggle={this.toggle} size="lg" className="p-0" style={{backgroundColor:"tarnsparent!important"}}>
                                            
                                            <ModalBody >
                                            <button className="close" onClick={this.toggle}>&times;</button>
                                            <img src={this.state.imagesrc} alt="" width="100%" height="100%"/> 
                                            </ModalBody>
                                            </Modal>
                                    </div>
                                   )
                               })}
                                    
                               </div>
                           
                               </div>
                               </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default ShowProject;