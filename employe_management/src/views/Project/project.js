import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Link } from 'react-router-dom'
import Moment from 'react-moment';
import config from '../../port.js';
import './project.css';
import inProgress from '../../assets/images/progress.png'
import completed from '../../assets/images/completed.png'
import notStarted from '../../assets/images/not-started.png'

class Project extends Component{
    constructor(props){
        super(props)
        this.state = ({projects:[],add:false,image:""})
       
    }

    componentDidMount(){ 
     axios.get(config.port+"/users/project").then(res=>{
         console.log(res.data)
        //let src = `${config.port}/images/${res.data.document[0][0]}`
        //console.log(src)
        // this.setState({image:src})
        this.setState({projects:res.data})
     })
    }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Project Management
                            <Link to='/project/add'>

                            <Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}><i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
                            </h4>
                        </CardHeader>
                        <CardBody>
                        <div className="row">
                                {this.state.projects.map((project,key)=>{
                                    let showlink = `project/show/${project.id}`
                                    let editlink = `project/edit/${project.id}`
                                    let clientlink = `client/show/${project.client_id}`
                                    return(
                                        
                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3 p-2 project-div">
                                            <h6><Link to={showlink}><b>{Capitalize(project.name)} </b></Link>
                                            <Link to={editlink}><i className="fa fa-pencil" style={{float:"right"}} ></i></Link></h6>
                                                <Link to={showlink}>{project.status === "in_progress" ?<img src={inProgress} className="my-2" alt="" width="50px" height="40px" />:""}</Link>
                                                <Link to={showlink}>{project.status === "not_started" ?<img src={notStarted} className="my-2" alt="" width="50px" height="40px" />:""}</Link>
                                                <Link to={showlink}>{project.status === "completed" ?<img src={completed} className="my-2" alt="" width="50px" height="40px" />:""}</Link>

                                            <p><Link to={showlink}><b>Start Date : </b><Moment format="DD-MMMM-YYYY">{project.start_date}</Moment></Link><br />
                                            <Link to={clientlink}> <b>Client Name : </b>{project.client_name}</Link></p>
                                        </div>
                                       
                                    )
                                })}
                            </div>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default Project;