import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddProject from './addproject';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddProject /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});