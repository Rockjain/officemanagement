import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import config from '../../port.js';

class AddClient extends Component{
    constructor(props){
        super(props)
        this.state = ({name:"",address_1:"",address_2:"", city:"",state:"", country:"",pincode:"",contact_no_1:"",
        contact_no_2:"",email_1:"",email_2:" ", skype:" ", other_detail_1:"", other_detail_2:"", other_detail_3:"", other_detail_4:"",
        redirect:false})
       
    }

    componentDidMount(){ 
     
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      
      console.log(this.state)
    }

    addclient=(event)=>{
        event.preventDefault();
        axios.post(config.port+"/users/addclient",{
            name:this.state.name,
            address_line_1:this.state.address_1,
            address_line_2:this.state.address_2,
            city:this.state.city,
            state:this.state.state,
            country:this.state.country,
            pincode:this.state.pincode,
            contact_no_1:this.state.contact_no_1,
            contact_no_2:this.state.contact_no_2,
            email_1:this.state.email_1,
            email_2:this.state.email_2,
            skype:this.state.skype,
            other_detail_1:this.state.other_detail_1,
            other_detail_2:this.state.other_detail_2,
            other_detail_3:this.state.other_detail_3,
            other_detail_4:this.state.other_detail_4,
        }).then(res=>{
            if(res.data.success === true){
                this.setState({redirect:true})
            }
        })
    }

    renderRedirect=()=>{
        if(this.state.redirect){
          return <Redirect to="/client" />
        }
    }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Add Client</h4>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={this.addclient}>
                                <Table borderless>
                                    <tr>
                                        <th>Name : </th>
                                        <td><input type="text" name="name" placeholder="Client Name" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Address Line 1 : </th>
                                        <td><input type="text" name="address_1" placeholder="Address Line 1" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Address Line 2 : </th>
                                        <td><input type="text" name="address_2" placeholder="Address line 2" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>City : </th>
                                        <td><input type="text" name="city" placeholder="City" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>State : </th>
                                        <td><input type="text" name="state" placeholder="State" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Country : </th>
                                        <td><input type="text" name="country" placeholder="Country" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Pincode : </th>
                                        <td><input type="text" name="pincode" placeholder="Pincode" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Contact Number : </th>
                                        <td><input type="text" name="contact_no_1" placeholder="Contact Number 2" className="p-1 w-25 mr-3" onChange={this.myChangeHandler}/>
                                        <input type="text" name="contact_no_2" placeholder="Contact Number 2" className="p-1 w-25" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Email Id : </th>
                                        <td><input type="text" name="email_1" placeholder="Email Id 1" className="p-1 mb-2 w-75" onChange={this.myChangeHandler}/>
                                        <input type="text" name="email_2" placeholder="Email Id 2" className="p-1 w-75" onChange={this.myChangeHandler}/></td>
                                    </tr>
                                    <tr>
                                        <th>Skype Id : </th>
                                        <td><input type="text" name="skype" placeholder="Skype id" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <th>Other contact Deatil : </th>
                                        <td><input type="text" name="other_detail_1" placeholder="Other Contact Detail 1" className="p-1 mb-2 w-75" onChange={this.myChangeHandler}/>
                                        <input type="text" name="other_detail_2" placeholder="Other Contact Detail 2" className="p-1 mb-2 w-75" onChange={this.myChangeHandler}/>
                                        <input type="text" name="other_detail_3" placeholder="Other Contact Detail 3" className="p-1 mb-2 w-75" onChange={this.myChangeHandler}/>
                                        <input type="text" name="other_detail_4" placeholder="Other Contact Detail 4" className="p-1 w-75" onChange={this.myChangeHandler}/> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"  className="w-50">
                                        {this.renderRedirect()}
                                        <Button color="primary" type="submit" className="float-right mr-5">Add</Button>
                                        </td>
                                        </tr>
                                </Table>
                            
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default AddClient;