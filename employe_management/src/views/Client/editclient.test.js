import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import EditClient from './editclient';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <EditClient match={{params: {id: "1"}, isExact: true, path: "/editclient/:id", name: "Edit Client"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Client</strong>)).toEqual(true)
  wrapper.unmount()
});