/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom'
import Moment from 'react-moment';
import config from '../../port.js';

class Client extends Component{
    constructor(props){
        super(props)
        this.state = ({client:[],add:false,delete:true, login:false})
       
    }

    componentDidMount(){
        if(localStorage.getItem("token")){
        axios.get(config.port+"/users/showclient").then(res=>{
            console.log(res.data)
            this.setState({client:res.data})
        })
    }else{
        this.setState({login:true})
    }
    }

    delete=(id)=>{
        console.log(id)
        axios.delete(config.port+"/users/deleteclient",{params:{id:id}}).then(res=>{
            if(res.data.success === true){
                axios.get(config.port+"/users/showclient").then(res=>{
                    console.log(res.data)
                    this.setState({client:res.data})
                })
            }
        })
    }

    onchangecheckbox=()=>{
        var ref = document.getElementById("client")
        let checkclient =0
        var getclient = ref.getElementsByClassName('d-check')
        for(let i=0;i<getclient.length;i++){
            
            if(getclient[i].checked === true){
                checkclient++
                
              }
             
        }
        if(checkclient >0){
            this.setState({add:true,delete:false}) 
        }else{
            this.setState({add:false,delete:true})
        }
    }

    multipledelete=()=>{
        var ref = document.getElementById("client")
       let ids=[];
        var getclient = ref.getElementsByClassName('d-check')
        for(let i=0;i<getclient.length;i++){
            
            if(getclient[i].checked === true){
                let id={id:getclient[i].id}
                ids.push(id);
              }
             
        }
        console.log(ids)
        let cid = JSON.stringify(ids)
        axios.delete(config.port+"/users/deletemultipleclient",{params:{ids:cid}}).then(res=>{
            if(res.data.success === true){
                for(let i=0;i<getclient.length;i++){
                    getclient[i].checked = false  
                }

                this.setState({add:false,delete:true})
    
                axios.get(config.port+"/users/showclient").then(res=>{
                    console.log(res.data)
                    this.setState({client:res.data})
                })

                }
        })
    }

    search=(event)=>{

        let val = event.target.value;
        //console.log(nam,val)
        var queryResult=[];
        if(val !== ""){
        axios.get(config.port+"/users/searchclient",{params:{val:val}}).then(res=>{
      
            res.data.map((rows,index)=>{
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({client:queryResult})
        })
        }
    }

    renderLogin=()=>{
        if (this.state.login) {
          return <Redirect to='/login' />
        }
      }

    render(){
         const Capitalize=(str)=>{
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                {this.renderLogin()}
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Client Management
                            <Link to='/client/add'>

                            <Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}><i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>

                            <a className="btn btn-danger btn-sm ml-2 float-right" hidden={this.state.delete} onClick={this.multipledelete}><i className="fa fa-trash" style={{color:"white"}} ></i></a>

                            </h4>
                        </CardHeader>
                        <CardBody>
                            <Form id="client">
                            <div>
                                <InputGroup className="mb-2">
                                        <InputGroupAddon addonType="prepend"><Button type="button"><i className="fa fa-search"></i></Button></InputGroupAddon>
                                        <Input placeholder="Search" className="w-75" name="searchbar" onChange={this.search}/> 
                                                                        
                                        <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler}>
                                            <option value="10">10</option>
                                            {this.state.client.length>10?<option value="15">15</option>:<span> </span>}
                                            {this.state.client.length>15?<option value="20">20</option>:<span> </span>}
                                            {this.state.client.length>20?<option value="25">25</option>:<span> </span>}
                                            {this.state.client.length>25?<option value="30">30</option>:<span> </span>}
                                        </Input>
                                                            
                                        </InputGroup>
                                                        
                                    </div>
                            <Table hover responsive>
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Client Name</th>
                                        <th>Contact Number</th>
                                        <th>Email Id</th>
                                        <th>Skype Id</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.client.map((client,key)=>{
                                        let editclient = `client/edit/${client.id}`
                                        let clientlink = `client/show/${client.id}`
                                        return (
                                            <tr key={key}>
                                                <td> <input type="checkbox" className="mr-2 d-check" value={client.id} id={client.id}  onChange={this.onchangecheckbox}/>
                                                <Link to={clientlink}>{key+1}</Link>
                                                 </td>
                                                <td><Link to={clientlink}>{Capitalize(client.name)}</Link></td>
                                                <td>{client.contact_no_1} {client.contact_no_2 !== 0 ? ","+client.contact_no_2:""}</td>
                                                <td>{client.email_1+","} {" "+client.email_2} </td>
                                                <td>{client.skype_id} </td>
                                                <td> <Link to={editclient}><Button name="editbtn" id="editbtn" className="btn-sm" color="primary" type="button">
                                                    <i className="fa fa-pencil" style={{color:"white"}}></i>
                                                    </Button></Link>

                                                    <a className="btn btn-danger btn-sm ml-2" onClick={()=>this.delete(client.id)}><i className="fa fa-trash" style={{color:"white"}} ></i></a>    
 </td>
                                              </tr>
                                        )
                                    })}
                                </tbody>
                            </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default Client;