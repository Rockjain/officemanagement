import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import config from '../../port.js';

class ShowClient extends Component{
    constructor(props){
        super(props)
        this.state = ({client:[],contact:[]})
       
    }

    componentDidMount(){ 
        axios.get(config.port+"/users/clientbyid",{params:{id:this.props.match.params.id}}).then(res=>{
            console.log(res.data)
            let contact = res.data.contact_details;
            let contact_array=[];
            for(var i in contact) {
                contact_array.push(contact[i]);
            }
            console.log(contact_array)
            this.setState({client:res.data.rows, contact:contact_array})
        })
    }

    render(){
         const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }
    let editclient = ``
if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col lg="9">
                        <Card>
                        <CardHeader>
                            <h4>Client Detail
                            <Link to={"/client/edit/"+this.props.match.params.id} style={{color:"black"}}><i className="fa fa-pencil float-right"></i></Link>
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <Form >
                                <Table borderless striped>
                                {this.state.client.map((client, key)=>{
                                    return(
                                        <tbody>
                                            <tr>
                                                    <th>Id</th>
                                                    <td>{client.id}</td>
                                                </tr>
                                                <tr>
                                                    <th>Name</th>
                                                    <td>{client.name} </td>
                                                </tr>
                                                <tr>
                                                    <th>Address</th>
                                                    <td>{client.address_line_1} {client.address_line_2} {client.city} {client.state} {client.country} {client.pin_code} </td>
                                                </tr>
                                                <tr>
                                                    <th>Country </th>
                                                    <td>{client.country}</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact No.</th>
                                                    <td>{client.contact_no_1}  {client.contact_no_2 !== 0? ","+client.contact_no_2:""}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email Id</th>
                                                    <td>{client.email_1} {client.email_2}</td>
                                                </tr>
                                                <tr>
                                                    <th>Skype Id</th>
                                                    <td>{client.skype_id} </td>
                                                </tr>
                                                <tr>
                                                    <th>Other contact_details</th>
                                                    <td>
                                                        <Table borderless>
                                                        {this.state.contact.map((contact,key)=>{
                                                        return(
                                                            <tr>
                                                                <td>{key+1}</td>
                                                                <td>{contact}</td>
                                                            </tr>
                                                        )
                                                        })}
                                                        </Table>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    )
                                })}
                                    
                                </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default ShowClient;