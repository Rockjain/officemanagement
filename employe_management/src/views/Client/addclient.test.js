import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddClient from './client';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddClient /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});