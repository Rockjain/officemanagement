/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Table, Form, Button} from 'reactstrap';
import Moment from  'react-moment';
import { Redirect } from 'react-router-dom'
import config from '../../port.js';

class EditTask extends Component{
    constructor(props){
        super(props)
        this.state = ({name:"",date:"", time:"", project:"", task:"", status:" ",redirect:false})
     
    }

    componentDidMount(){ 
        let id = this.props.match.params.id;
        console.log(id)
        axios.get(config.port+"/users/showtaskbyid",{params:{id:id}}).then(res=>{
            console.log(res.data)
            let task = res.data[0]
           this.setState({name:task.name, time:task.time, project:task.project, task:task.task, status:task.status, date:task.date})
        })
    }

    
    myChange = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
        //console.log(this.state)
      }

    submit=(event)=>{
        event.preventDefault();
        console.log(this.state);
        let id = this.props.match.params.id;
        axios.post(config.port+"/users/updateTasktable",{
            project:this.state.project,
            task:this.state.task,
            time:this.state.time,
            status:this.state.status,
            id:id
        }).then(res=>{
            if(res.data.success === true){
                this.setState({redirect:true})
            }
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/task' />
        }
      }

    render(){

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Employee : {this.state.name} <Moment format="MMMM DD, YYYY" className="float-right">{this.state.date}</Moment>
                               </h4>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={this.submit}>
                            <Table borderless>
                              
                               <tr>
                                   <th>Project :</th>
                                    <td><input name="project" value={this.state.project} onChange={this.myChange} className="w-75"/></td>
                              </tr>
                              <tr>
                                   <th>Task :</th>
                                    <td><input name="task" value={this.state.task} onChange={this.myChange} className="w-75"/></td>
                              </tr>
                              <tr>
                                   <th>Time :</th>
                                    <td><input name="time" value={this.state.time} onChange={this.myChange} className="w-75"/></td>
                              </tr>
                              <tr>
                                   <th>Status :</th>
                                    <td>
                                        {this.state.status === "working"?<input type="radio"name="status" value={this.state.status} checked onChange={this.myChange}/>: <input type="radio"  name="status" value="working" onChange={this.myChange}/>} Working 
                                        {this.state.status === "completed"? <input type="radio" name="status" value={this.state.status} checked onChange={this.myChange}  className="ml-3" />: <input type="radio"  className="ml-3" name="status" value="completed" onChange={this.myChange}/>} Completed </td>
                              </tr>
                              <tr>
                                  <td colspan="2">
                                  {this.renderRedirect()}
                                  <Button type="submit" color="primary" className="float-right">Submit</Button>
                                  </td>
                                  </tr>
                            </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default EditTask;