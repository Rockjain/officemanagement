import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Task from './task';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Task /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});