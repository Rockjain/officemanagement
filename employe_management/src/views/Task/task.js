/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Form, Input, InputGroup, InputGroupAddon,Table} from 'reactstrap';
import Moment from 'react-moment';
import { Link } from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Pagination from "react-js-pagination";
import config from '../../port.js';


class Task extends Component{
    constructor(props){
        super(props)
        this.state = ({tasks:[],add:false,delete:true,task:[],edit:[],shown:[],startDate:new Date(),activePage: 1,pageSize:10,pageNumber:"",currentIndex:1,items:[],})
      
    }

   // port = "http://localhost:3002"

      refreshItems=()=>{
        let items = this.state.task.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
       // let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({items:items})
        console.log("item",this.state.items,this.state.pageSize)
      }

    componentDidMount(){ 
        axios.get(config.port+"/users/showtask").then(res=>{
            console.log(res.data)
            this.setState({tasks:res.data})
            this.refreshItems();
            let task=[];
            let edit=[];
            let shown=[];
            res.data.map((data,ind)=>{
                task.push(data.task);
                edit.push(true)
                shown.push(false)
            })
            this.setState({task:task,edit:edit,shown:shown})
        })
    }

    handlePageChange=(pageNumber)=>{
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage:pageNumber,currentIndex:pageNumber});
        console.log("index",this.state.currentIndex)
       this.refreshItems();
       var ref = document.getElementById("task")
                   
       var getTask = ref.getElementsByClassName('d-check')
       for(let i=0;i<  getTask.length;i++){
        getTask[i].checked = false  
        }

      }

      myChangeHandler = (event) => {
        let val = event.target.value;
        console.log("val",val)
        this.setState({pageSize:val,currentIndex:1});
        this.refreshItems();
        console.log(this.state.pageSize)
    }

    handleCheck=(id)=>{
        axios.post(config.port+"/users/updateTasksSatus",{id:id}).then(res=>{
            if(res.data.success === true){
                axios.get(config.port+"/users/showtask").then(res=>{
            console.log(res.data)
            this.setState({tasks:res.data})
        }) 
            }
        })
    }

    handleChange=(date)=>{
        this.setState({
          startDate: date
        });

        axios.get(config.port+"/users/searchtaskbydate",{params:{date:date}}).then(res=>{
            console.log(res.data)
            this.setState({tasks:res.data})
        })
      }

      handleSelect=(date)=>{
        this.setState({
          startDate: date
        });
      }

    deletetask=(id)=>{
        axios.delete(config.port+"/users/deletetask",{params:{id:id}}).then(res=>{
            if(res.data.success === true){
                axios.get(config.port+"/users/showtask").then(res=>{
            console.log(res.data)
            this.setState({tasks:res.data})
            this.refreshItems();
            var ref = document.getElementById("task")
                   
            var getTask = ref.getElementsByClassName('d-check')
            for(let i=0;i<  getTask.length;i++){
             getTask[i].checked = false  
             }
        }) 
            }
        })
    }

    multipledelete=()=>{
        var ref = document.getElementById("task")
        let ids=[];
         var gettask = ref.getElementsByClassName('d-check')
         for(let i=0;i<gettask.length;i++){
             
             if(gettask[i].checked === true){
                 let id={id:gettask[i].id}
                 ids.push(id);
               }
              
         }
         console.log(ids)
         let tid = JSON.stringify(ids)
         axios.delete(config.port+"/users/deletemultipletask",{params:{ids:tid}}).then(res=>{
             if(res.data.success === true){
                 for(let i=0;i<gettask.length;i++){
                     gettask[i].checked = false  
                 }
 
                 this.setState({add:false,delete:true})
                 axios.get(config.port+"/users/showtask").then(res=>{
                    console.log(res.data)
                    this.setState({tasks:res.data})
                     this.refreshItems();
                }) 
             
             }
         })
    }

    onchangecheckbox=()=>{
        
        var ref = document.getElementById("task")
        let checkdepartment =0
        var gettask = ref.getElementsByClassName('d-check')
        for(let i=0;i<gettask.length;i++){
            
            if(gettask[i].checked === true){
                checkdepartment++
                
              }
             
        }
        if(checkdepartment >0){
            this.setState({add:true,delete:false}) 
        }else{
            this.setState({add:false,delete:true})
        }
    }

    search=(event)=>{
        
        let val = event.target.value;
        //console.log(nam,val)
        var queryResult=[];
        if(val !== ""){
        axios.get(config.port+"/users/searchtask",{params:{val:val}}).then(res=>{
      
            res.data.map((rows,index)=>{
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({tasks:queryResult})
        })
        }else{
            this.refreshItems();
        }
    }

    edit=idx=>evt=>{
    evt.preventDefault();
    let edittask = this.state.edit.map((task,sidx)=>{
        if(idx === sidx){
            return false;
        }else{
            return true;
        }
    })
    let showntask = this.state.shown.map((task,sidx)=>{
        if(idx === sidx){
            return true;
        }else{
            return false;
        }
    })
    console.log(edittask)
    this.setState({edit:edittask,shown:showntask})
    }

    
    edittask=(idx,id)=>evt=>{
        
        const task = this.state.task.map((task, sidx) => {
            if (idx === sidx){
                    if(evt.key === "Enter"){
                    if(evt.target.value !== ""){
                            console.log(id,idx)
                            console.log(evt.target.value)
                            axios.post(config.port+"/users/updatetask",{task:evt.target.value,id:id}).then(res=>{
                                console.log(res.data)
                                if(res.data.success===true){
                                    axios.get(config.port+"/users/showtask").then(res=>{
                                            console.log(res.data)
                                            this.setState({tasks:res.data})
                                            let task=[];
                                            let edit=[];
                                            let shown=[];
                                            res.data.map((data,ind)=>{
                                                task.push(data.task);
                                                edit.push(true)
                                                shown.push(false)
                                            })
                                            this.setState({task:task,edit:edit,shown:shown})
                                        })

                                    let edittask = this.state.edit.map((task,sidx)=>{
                                        if(idx === sidx){
                                            return true;
                                        }else{
                                            return true;
                                        }
                                    })
                                    let showntask = this.state.shown.map((task,sidx)=>{
                                        if(idx === sidx){
                                            return false;
                                        }else{
                                            return false;
                                        }
                                    })
                                    console.log(edittask)
                                    this.setState({edit:edittask,shown:showntask})
                                }
                            })
                           
                        }  
                }
                return evt.target.value;
                }else{
                    return task 
                }            
          });

          this.setState({task:task});
    }

    render(){
    const Capitalize=(str)=>{
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    const paginationContent = (
        <div className="ml-5">
         <Pagination
            hideDisabled
            prevPageText='prev'
            nextPageText='next'
            firstPageText='first'
            lastPageText='last'
            activePage={this.state.activePage}
            itemsCountPerPage={this.state.pageSize}
            totalItemsCount={this.state.task.length}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange}
            />
          </div>
      )

      var pagination;
      if(this.state.task.length > this.state.pageSize){
        pagination = <div>{paginationContent}</div>
      }else{
        pagination = <div></div>
      }

    if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Assigned Task
                                <Link to="/task/addtask"><Button color="secondary" outline size="sm" type="button" style={{float:"right"}} hidden={this.state.add}>Add Task</Button></Link>
                                <a className="btn btn-danger btn-sm ml-2 float-right" hidden={this.state.delete} onClick={this.multipledelete}><i className="fa fa-trash" style={{color:"white"}} ></i></a>
                                </h4>
                        </CardHeader>
                        <CardBody>
                            <Form id="task">
                            
                            <div>
                                                        <InputGroup className="mb-2">
                                                                <InputGroupAddon addonType="prepend"><Button type="button"><i className="fa fa-search"></i></Button></InputGroupAddon>
                                                                <Input placeholder="Search" className="w-50" name="searchbar" onChange={this.search}/> 
                                                                <DatePicker
                                                                        selected={this.state.startDate}
                                                                        onChange={this.handleChange}
                                                                        onSelect={this.handleSelect}
                                                                        dateFormat="MMMM dd, yyyy"
                                                                        className="pt-1 pl-2 pb-2 border border-light"/> 
                                                                <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler}>
                                                                    <option value="10">10</option>
                                                                    {this.state.tasks.length>10?<option value="15">15</option>:<span> </span>}
                                                                    {this.state.tasks.length>15?<option value="20">20</option>:<span> </span>}
                                                                    {this.state.tasks.length>20?<option value="25">25</option>:<span> </span>}
                                                                    {this.state.tasks.length>25?<option value="30">30</option>:<span> </span>}
                                                                    </Input>
                                                                    
                                                            </InputGroup>
                                                        
                                                       </div>
                             <div style={{overflowX:"auto"}}>  
                            <Table >
                               <thead>
                                <tr>
                                <th>S.No.</th>
                                <th>Employee name</th>
                                <th>Date </th>
                                <th>Project </th>
                                <th>Task </th>
                                <th>Time</th>
                                <th>Status </th>
                                <th>Action</th>
                                </tr>
                               </thead>
                               <tbody>
                                    {this.state.tasks.map((task,key)=>{
                                      var checked;
                                      task.status ==="completed"? checked=true:checked=false;
                                      let taskname=`task-${key}`
                                      let editlink=`task/edit/${task.id}`
                                        return(
                                            <tr key={key}>
                                                <td> <input type="checkbox" className="mr-2 d-check" value={task.id} id={task.id}  onChange={this.onchangecheckbox}/>
                                                {key+1}</td>
                                                <td>{Capitalize(task.name)} </td>
                                                <td><Moment format="DD-MM-YYYY">{task.date}</Moment></td>
                                                <td>{Capitalize(task.project)}</td>
                                                <td><span hidden={this.state.shown[key]} onClick={this.edit(key)} >{Capitalize(task.task)}</span>

                                                <input name={taskname} value={this.state.task[key]} hidden={this.state.edit[key]} onChange={this.edittask(key,task.id)} onKeyPress={this.edittask(key,task.id)}/>
                                                
                                                </td>
                                                <td>{task.time} </td>
                                                <td>  {task.status === "completed"?<Button type="button" color="primary" size="sm" disabled>{Capitalize(task.status)}</Button>:<Button type="button" color="danger" size="sm" onClick={()=>this.handleCheck(task.id)} hidden={checked}>Mark As Complete</Button>} </td>
                                                <td>
                                                    <Link to={editlink}><Button name="editbtn" id="editbtn" className="btn-sm" color="primary" type="button">
                                                    <i className="fa fa-pencil" style={{color:"white"}}></i>
                                                    </Button></Link>

                                                    <a className="btn btn-danger btn-sm ml-2" onClick={()=>this.deletetask(task.id)}><i className="fa fa-trash" style={{color:"white"}} ></i></a>    

                                                </td>
                                            </tr>
                                        )
                                    })}
                                   </tbody>
                            </Table>
                            </div>
                            <div className="ml-5">
                                                      {pagination}
                                                    </div>
                            
                             </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default Task;