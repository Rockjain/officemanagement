import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddDepartment from './adddepartment';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddDepartment /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});