/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable array-callback-return */
import React, {Component} from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table,Button } from 'reactstrap';
import axios from 'axios';
import { Redirect,Link } from 'react-router-dom';
import config from '../../port.js';

class EditDepartment extends Component {
  constructor(props){
    super(props); 
    this.state =({department:"",parent_id:"",maindepartment:[],main:"",departmentlink:"",redirect:false});
  }

  componentDidMount(){
    axios.get(config.port+"/users/department").then(res=>{
          //console.log(res.data) 
          let maindepartment=[];
          res.data.map((data,index)=>{
             
                  if(data.parent_id === 0){
                      maindepartment.push(data)
                  }
              
          })
        
       // console.log(this.state.department)
       const department = res.data.find( department => department.id.toString() === this.props.match.params.id)
       const mainid = maindepartment.find( main => main.id === department.parent_id) 
       console.log(department.id,mainid)
       this.setState({department:department.name,parent_id:department.parent_id,maindepartment:maindepartment})

       if(mainid !== undefined){
        this.setState({main:mainid.name})
       }else{

       }
      
    })
    
  }

   myChangeHandler = (event) => {
       
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});  
    }

  editDepartment=(event)=>{
    event.preventDefault();
    axios.post(config.port+"/users/editdepartment",{id:this.props.match.params.id,name:this.state.department,parent_id:this.state.parent_id}).then(res=>{
        console.log(res.data)
        if(res.data.success === true){
           this.setState({redirect:true})
        }
    })
  console.log(this.state.name)
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to='/department' />
    }
  }

  render() {
   
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={9}>
            <Card>
              <CardHeader>
                <strong><i className="fa fa-edit pr-1"></i> &nbsp; Department id: {this.props.match.params.id}</strong>
              </CardHeader>
              <CardBody>
              <form onSubmit={this.editDepartment}>
                            <Table borderless>
                                <tbody>
                                <tr>
                                    <td>
                                        Department Name
                                    </td>
                                    <td>
                                       <input type="text" name="department" className="w-75 my-1" value={this.state.department} onChange={this.myChangeHandler}/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       Main department
                                    </td>
                                    <td>
                                       <select name="parent_id" id="parent_id" className="w-75 my-1" onChange={this.myChangeHandler}>

                                           {this.state.parent_id === 0 ? <option value={this.state.parent_id}>Main Department</option>:<><option value={this.state.parent_id}>{this.state.main}</option><option value="0">Main Department</option></>} 

                                            {this.state.maindepartment.map((data,key)=>{
                                                return <option value={data.id}>{data.name} </option>
                                            })}
                                       </select>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                            {this.renderRedirect()}
                           <Button color="primary" type="submit" style={{float:"right"}} className="ml-3">Edit</Button>

                           <Link to="/department" className="text-white"><a className="btn btn-danger" type="button" style={{float:"right"}}>Cancel</a></Link>
                            </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default EditDepartment;