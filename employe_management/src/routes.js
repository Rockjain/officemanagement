import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));
const profile = React.lazy(()=>import('./views/profile/profile'))
const AddEmployee = React.lazy(()=>import('./views/employee/Add'))
const ShowEmployee = React.lazy(()=>import('./views/employee/show'))
const Employee = React.lazy(()=>import('./views/employee/show/employee'))
const EditEmployee = React.lazy(()=>import('./views/employee/show/editemployee'))
const Department = React.lazy(()=>import('./views/Department/departments'))
const AddDepartment = React.lazy(()=>import('./views/Department/adddepartment'))
const EditDepartment = React.lazy(()=>import('./views/Department/editdepartment'))
const Salary = React.lazy(()=>import('./views/Salary/Salary_setup/salary'))
const AddSalary = React.lazy(()=>import('./views/Salary/Salary_setup/addsalary'))
const EditSalary = React.lazy(()=>import('./views/Salary/Salary_setup/editsalary'))
const SalaryTransaction = React.lazy(()=>import('./views/Salary/Salary_transaction/salarytransaction'))
const GenerateSalary = React.lazy(()=>import('./views/Salary/Salary_transaction/generate'))
const SalaryReport = React.lazy(()=>import('./views/SalaryReports/report'))
const FinalReport = React.lazy(()=>import('./views/FinalReport/report'))
const Task = React.lazy(()=>import('./views/Task/task'))
const AddTask = React.lazy(()=>import('./views/Task/addtask'))
const EditTask = React.lazy(()=>import('./views/Task/edittask'))
const Client = React.lazy(()=>import('./views/Client/client.js'))
const AddClient = React.lazy(()=>import('./views/Client/addclient.js'))
const Project = React.lazy(()=>import('./views/Project/project'))
const AddProject = React.lazy(()=>import('./views/Project/addproject'))
const TaskManagement = React.lazy(()=>import('./views/TaskManagement/taskmange'))
const EditClient = React.lazy(()=>import('./views/Client/editclient'))
const ShowClient = React.lazy(()=>import('./views/Client/show'))
const ShowProject = React.lazy(()=>import('./views/Project/show'))
const EditProject = React.lazy(()=>import('./views/Project/edit'))

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/addemployee',exact: true, name:'Addemployee', component: AddEmployee},
  { path: '/employee',exact: true, name:'Employees', component: ShowEmployee},
  { path: '/employee/show/:id',exact: true, name:'Employee Details', component: Employee },
  { path: '/employee/edit/:id', exact:true, name:'Edit Employee', component:EditEmployee },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/profile', exact:true, name:'Profile', component: profile },
  { path: '/department', exact:true, name:'Department', component: Department },
  { path: '/department/add', exact:true, name:'Add Department', component: AddDepartment },
  { path: '/department/:id', exact:true, name:'Edit Department', component: EditDepartment },
  { path: '/salary', exact:true, name:'Salary Setup', component:Salary },
  { path: '/salary/add', exact:true, name:'Add Employee Salary', component:AddSalary},
  { path: '/salary/edit/:id', exact:true, name:'Upadte Salary', component:EditSalary },
  { path: '/transaction', exact:true, name:'Salary', component:SalaryTransaction },
  { path: '/transaction/generate', exact:true, name:'Generate Salay', component:GenerateSalary },
  { path: '/report', exact:true, name:"Report", component:SalaryReport },
  { path: '/report/salaryreport', exact:true, name:"Salary Report", component:SalaryReport },
  { path: '/report/finalreport', exact:true, name:" FinalRaports", component:FinalReport },
  { path: '/task', exact:true, name:"Task", component:Task },
  { path: '/task/addtask', exact:true, name:"Add Task", component:AddTask },
  { path: '/task/edit/:id', exact:true, name:"Edit task", component:EditTask },
  { path: '/client', exact:true, name:"Client", component:Client },
  { path: '/client/add', exact:true, name:"Add Client", component:AddClient },
  { path: '/project', exact:true, name:"Project", component:Project },
  { path: '/project/show/:id', exact:true, name:"Project Detail", component:ShowProject },
  { path: '/project/edit/:id', exact:true, name:"Edit Project", component:EditProject },
  { path: '/project/add', exact:true, name:"Add Project", component:AddProject },
  { path: '/taskmanage', exact:true, name:"Task management", component:TaskManagement },
  { path: '/client/edit/:id', exact:true, name:"Edit Client", component:EditClient },
  { path: '/client/show/:id', exact:true, name:"Client Detail", component:ShowClient },

];

export default routes;
